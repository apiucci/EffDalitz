/*!
 *  @file      effLUT.cpp
 *  @author    Alessio Piucci
 *  @brief     Script to retrieve the efficiency value for a given candidate, reading from lookup tables.
 *  @return    Returns the efficiency value.                   
 */

#include "effLUT.h"

//----------------------------------------------
// Get efficiency as function of full kinematics of the event
//----------------------------------------------
void effLUT::GetEventEff(const double Kin_1, const double Kin_2,
                         const std::map<std::string, std::pair<double, double>> BDT_vars,
                         const std::map<std::string, TLorentzVector> PIDqvects,
                         const unsigned int nTracks, const bool varxP_varyEta,
                         Efficiency &FiducialEff, Efficiency &GeneratorEff, Efficiency &TriggerToSelEff,
                         Efficiency &KinEff_tot, Efficiency &BDTEff_tot, Efficiency &PIDEff_tot,
                         Efficiency &comb_eff, const std::string &year){
  
  //----------------------------//
  //  compute the efficiencies  //
  //----------------------------//
  
  //kinematic efficiency
  GetKineEff(Kin_1, Kin_2, FiducialEff, GeneratorEff, TriggerToSelEff, KinEff_tot, year);
  
  //BDT efficiency
  GetBDTEff(BDT_vars, BDTEff_tot, year);
  
  //PID efficiency
  GetPIDEff(PIDqvects, nTracks, varxP_varyEta, PIDEff_tot, year);
  
  //check the signal efficiencies one more time
  if((!CheckValidEfficiency(KinEff_tot))
     || (!CheckValidEfficiency(BDTEff_tot))
     || (!CheckValidEfficiency(PIDEff_tot))){
        
    SetUnvalidEfficiency(comb_eff);
    
    return;
  }
  
  //----------------------------------------------------//
  //  finally, I can compute the combined efficiencies  //
  //----------------------------------------------------//
  
  CombineEfficiencies(comb_eff, std::vector<Efficiency>{KinEff_tot, BDTEff_tot, PIDEff_tot});
  
  //check for a fatal error, that should never happen
  if(!CheckValidEfficiency(comb_eff)){
    std::cout << "Error: what the hell: the single efficiencies were fine, but the combined one is unvalid."
              << " KinEff_tot = " << KinEff_tot.efficiency << " +- " << KinEff_tot.eff_err
              << ", BDTEff_tot = " << BDTEff_tot.efficiency << " +- " << BDTEff_tot.eff_err
              << ", PIDEff_tot = " << PIDEff_tot.efficiency << " +- " << PIDEff_tot.eff_err << std::endl;
    exit(EXIT_FAILURE);
  }

  return;
  
}

//---------------------------------------------------------
// Combine single efficiencies
//---------------------------------------------------------
void effLUT::CombineEfficiencies(Efficiency &TotEff, const std::vector<Efficiency> Eff_vect){
  
  //prepare the overall efficiency for the combination of the single efficiencies
  SetEfficiencyOne(TotEff);
  
  //now combine all PID cuts to get the combined efficiency and the number of total and passed events
  for(std::vector<Efficiency>::const_iterator it_vect = Eff_vect.begin();
      it_vect != Eff_vect.end(); ++it_vect){
    
    TotEff.efficiency *= (*it_vect).efficiency;
    
    TotEff.num_total += (*it_vect).num_total;
    TotEff.num_passed += (*it_vect).num_passed;
  }
  
  //finally combine the single errors, to get the final one
  //given the total efficiency E_tot = E_1 * E_2 * ... * E_n, the final error is:
  //sqrt( sum_i ( (E_tot/E_i) * dE_i)^2 )
  for(std::vector<Efficiency>::const_iterator it_vect = Eff_vect.begin();
      it_vect != Eff_vect.end(); ++it_vect){
    TotEff.eff_err += pow((*it_vect).eff_err * (TotEff.efficiency / (*it_vect).efficiency), 2.);
    TotEff.eff_err_low += pow((*it_vect).eff_err_low * (TotEff.efficiency / (*it_vect).efficiency), 2.);
    TotEff.eff_err_up += pow((*it_vect).eff_err_up * (TotEff.efficiency / (*it_vect).efficiency), 2.);
  }
  
  TotEff.eff_err = sqrt(TotEff.eff_err);
  TotEff.eff_err_low = sqrt(TotEff.eff_err_low);
  TotEff.eff_err_up = sqrt(TotEff.eff_err_up);
  
  return;
  
}

//---------------------------------------------------------
// Get a single efficiency from a TH2A object, with number of total and passed events
//---------------------------------------------------------
void effLUT::GetSingleEff(const double var_1, const double var_2,
                          TH2A *h_eff, Efficiency &Eff){
  
  if(h_eff == NULL){
    SetEfficiencyOne(Eff);
    return;
  }
  
  //find bin for these coordinates, but first check if the ranges are ok
  if( (var_1 < h_eff->GetMinX()) || (h_eff->GetMaxX() < var_1)
      || (var_2 < h_eff->GetMinY()) || (h_eff->GetMaxY() < var_2)
     ){
    
    log_errors_stream << "Warning: values out of histo " << h_eff->GetName()
                      << ", var_1 = " << var_1
                      << " [" << h_eff->GetMinX() << ", " << h_eff->GetMaxX() << "]" 
                      << ", var_2 = " << var_2
                      << " [" << h_eff->GetMinY() << ", " << h_eff->GetMaxY() << "]"
                      << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;
    
    SetUnvalidEfficiency(Eff);
    
    return;
  }
  
  // Find bin for these coordinates
  int i_bin = h_eff->FindBin(var_1, var_2);
  
  //efficiency
  Eff.efficiency = h_eff->GetBinContent(i_bin);
  
  //efficiency error
  Eff.eff_err = std::max(std::fabs(h_eff->GetBinErrorLow(i_bin)),
                         std::fabs(h_eff->GetBinErrorUp(i_bin)));
  Eff.eff_err_low = h_eff->GetBinErrorLow(i_bin);
  Eff.eff_err_up = h_eff->GetBinErrorUp(i_bin);
  
  //for the total and passed events, just use a reference value
  Eff.num_total = 100.;
  Eff.num_passed = 100.*Eff.efficiency;
  
  return;
}

//---------------------------------------------------------
// Get efficiency of kinematic selection
// from two different 2D efficiency distributions
//---------------------------------------------------------
void effLUT::GetKineEff(const double Kin_1, const double Kin_2,
                        Efficiency &FiducialEff, Efficiency &GeneratorEff, Efficiency &TriggerToSelEff,
                        Efficiency &KinEff_tot, const std::string &year){
  
  //fiducial efficiency
  if(Fiducial_Eff_on){
    if(Fiducial_eff_map.find(year) == Fiducial_eff_map.end()){
      std::cout << "GetKineEff:: Error: the selected year of the fiducial eff does not exist. year = "
                << year << std::endl;
      exit(EXIT_FAILURE); 
    }
    
    //get the fiducial efficiency
    FiducialEff = Fiducial_eff_map.at(year);
  }
  else
    SetEfficiencyOne(FiducialEff);
  
  //-------------------------------------//
  //  retrieve the generator efficiency  //
  //-------------------------------------//
  
  if(Generator_Eff_on){
    
    if(h_generator_eff_adaptive_map.find(year) == h_generator_eff_adaptive_map.end()){
      std::cout << "GetKineEff:: Error: the selected year of the generator eff does not exist. year = "
                << year << std::endl;
      exit(EXIT_FAILURE);
    }
    
    //get the efficiency
    GetSingleEff(Kin_1, Kin_2, h_generator_eff_adaptive_map.at(year), GeneratorEff);
    
    //check the generator efficiency
    if(!CheckValidEfficiency(GeneratorEff)){
      
      log_errors_stream << "Warning: Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
                        << ", GeneratorEff.eff = " << GeneratorEff.efficiency
                        << " +- " << GeneratorEff.eff_err
                        << ", GeneratorEff.num_total = " << GeneratorEff.num_total
                        << ", GeneratorEff.num_passed = " << GeneratorEff.num_passed
                        << ". Setting unvalid efficiency." << std::endl;
      
      SetUnvalidEfficiency(GeneratorEff);
      SetUnvalidEfficiency(KinEff_tot);
      
      return;
    }  //some checks
  }
  else
    SetEfficiencyOne(GeneratorEff);
  
  //----------------------------------------------//
  //  retrieve the trigger->selection efficiency  //
  //----------------------------------------------//
  
  if(TriggerToSel_Eff_on){
    
    if(h_TriggerToSel_eff_adaptive_map.find(year) == h_TriggerToSel_eff_adaptive_map.end()){
      std::cout << "GetKineEff:: Error: the selected year of the TriggerToSel eff does not exist. year = "
                << year << std::endl;
      exit(EXIT_FAILURE);  
    }
  
    //get the efficiency
    GetSingleEff(Kin_1, Kin_2, h_TriggerToSel_eff_adaptive_map.at(year), TriggerToSelEff);
    
    //check the trigger->selection efficiency
    if(!CheckValidEfficiency(TriggerToSelEff)){
      
      log_errors_stream << "Warning: Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
                        << ", TriggerToSelEff.eff = " << TriggerToSelEff.efficiency
                        << " +- " << TriggerToSelEff.eff_err
                        << ", TriggerToSelEff.num_total = " << TriggerToSelEff.num_total
                        << ", TriggerToSelEff.num_passed = " << TriggerToSelEff.num_passed
                        << ". Setting unvalid efficiency." << std::endl;
      
      SetUnvalidEfficiency(TriggerToSelEff);
      SetUnvalidEfficiency(KinEff_tot);
      
      return;
    }
  }
  else
    SetEfficiencyOne(TriggerToSelEff);
  
  //--------------------------------------//
  //  combine the kinematic efficiencies  //
  //--------------------------------------//
  
  CombineEfficiencies(KinEff_tot, std::vector<Efficiency>{FiducialEff, GeneratorEff, TriggerToSelEff});
  
  //check the total kinematic efficiency
  if(!CheckValidEfficiency(KinEff_tot)){
    log_errors_stream << "Warning: Kin_1 = " << Kin_1  << ", Kin_2 = " << Kin_2
                      << ", KinEff_tot.eff = " << KinEff_tot.efficiency
                      << " +- " << KinEff_tot.eff_err
                      << ", KinEff_tot.num_total = " << KinEff_tot.num_total
                      << ", KinEff_tot.num_passed = " << KinEff_tot.num_passed
                      << ". Setting unvalid efficiency." << std::endl;
    
    SetUnvalidEfficiency(KinEff_tot);
    
    return;  
  }
  
  return;
}

//-----------------------------------
// Get BDT efficiency for two cuts
//-----------------------------------
void effLUT::GetBDTEff(const std::map<std::string, std::pair<double, double>> BDT_vars,
                       Efficiency &BDTEff_tot, const std::string &year){

  //is the BDT efficiency switched on?
  if(!BDT_eff_on){
    SetEfficiencyOne(BDTEff_tot);

    return;
  }
  
  //check that all the particles and the selected year exist
  for(std::map<std::string, std::pair<double, double>>::const_iterator it_cut = BDT_vars.begin();
      it_cut != BDT_vars.end(); ++it_cut){

    //check the particle
    if(BDT_eff_adaptive_inception.find(it_cut->first) == BDT_eff_adaptive_inception.end()){
      std::cout << "GetBDTEff:: Error: the selected particle does not exist. part_name = "
                << it_cut->first << std::endl;
      exit(EXIT_FAILURE);
    }
    else{
      //check that the year exists, for the current particle
      if(BDT_eff_adaptive_inception.at(it_cut->first).find(year) == BDT_eff_adaptive_inception.at(it_cut->first).end()){
        std::cout << "GetBDTEff:: Error: the selected year does not exist. part_name = "
                  << it_cut->first << ", year = " << year << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }  //loop over the particles
  
  //vector of efficiency structure, only temporarely
  std::vector<Efficiency> BDTEff_vect;

  //loop over the BDT cuts, taken from the <part_name, qvects> map
  for(std::map<std::string, std::pair<double, double>>::const_iterator it_BDTcut = BDT_vars.begin();
      it_BDTcut != BDT_vars.end(); ++it_BDTcut){

    Efficiency BDTEff_vect_curr;

    //get the efficiency
    GetSingleEff(it_BDTcut->second.first,
                 it_BDTcut->second.second,
                 (BDT_eff_adaptive_inception.at(it_BDTcut->first)).at(year),
                 BDTEff_vect_curr);
    
    //check if the current efficiency is fine,
    //otherwise set all the efficiencies as unvalid
    if((!CheckValidEfficiency(BDTEff_vect_curr))
       || (!CheckValidEfficiency(BDTEff_vect_curr))){
      log_errors_stream << "Warning: BDTEff_vect_curr.eff = "
                        << BDTEff_vect_curr.efficiency
                        << " +- " << BDTEff_vect_curr.eff_err
                        << ", cut = " << it_BDTcut->first
                        << ", year = " << year
                        << ". Setting unvalid efficiency." << std::endl;
      
      SetUnvalidEfficiency(BDTEff_tot);
      
      return;
    }

    //add the current efficiency
    BDTEff_vect.push_back(BDTEff_vect_curr);
  }

  //--------------------------------//
  //  combine the BDT efficiencies  //
  //--------------------------------//
  
  CombineEfficiencies(BDTEff_tot, BDTEff_vect);
  
  if(!CheckValidEfficiency(BDTEff_tot)){
    log_errors_stream << "Error: what the hell: the single efficiencies were fine, but the combined one is unvalid."
                      << ". BDTEff_tot.efficiency = " << BDTEff_tot.efficiency
                      << " +- " << BDTEff_tot.eff_err << std::endl;
    
    exit(EXIT_FAILURE);
  }
  
  return;
}

		
//----------------------------- 
// Get efficiency for the combined PID cuts
//-----------------------------
void effLUT::GetPIDEff(const std::map<std::string, TLorentzVector> PIDqvects,
                       const double nTracks, const bool varxP_varyEta,
                       Efficiency &PIDEff, const std::string &year){
  
  //is the PID efficiency switched on?
  if(!PID_eff_on){
    SetEfficiencyOne(PIDEff);
    
    return;
  }
  
  //check that all the particles and the selected year exist
  for(std::map<std::string, TLorentzVector>::const_iterator it_cut = PIDqvects.begin();
      it_cut != PIDqvects.end(); ++it_cut){
    
    //check the particle
    if(PID_eff_adaptive_inception.find(it_cut->first) == PID_eff_adaptive_inception.end()){
      std::cout << "GetPIDEff:: Error: the selected particle does not exist. part_name = "
                << it_cut->first << std::endl;
      exit(EXIT_FAILURE);
    }
    else{
      
      //check that the year exists, for the current particle
      if(PID_eff_adaptive_inception.at(it_cut->first).find(year) == PID_eff_adaptive_inception.at(it_cut->first).end()){
        std::cout << "GetPIDEff:: Error: the selected year does not exist. part_name = "
                  << it_cut->first << ", year = " << year << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }  //loop over the particles
  
  //vector of efficiency structure, only temporarely
  std::vector<Efficiency> PIDEff_vect;
  
  //to handle efficiency maps with switched axis
  double var_x = -9999.;
  double var_y = -9999.;
  
  //find bin for these coordinates, but first check if the ranges are ok
  //test the x and y ranges on the first histo, it's fine
  //get<0>: zmin, get<1>: zmax, get<2>: TH2A histo
  
  //loop over the PID cuts, taken from the <part_name, qvects> map
  for(std::map<std::string, TLorentzVector>::const_iterator it_PIDcut = PIDqvects.begin();
      it_PIDcut != PIDqvects.end(); ++it_PIDcut){
    
    //set the variables on the x and y axis
    if(varxP_varyEta){
      var_x = it_PIDcut->second.P();
      var_y = it_PIDcut->second.PseudoRapidity();
    }
    else{
      var_x = it_PIDcut->second.PseudoRapidity();
      var_y = it_PIDcut->second.P();
    }
    
    //get the <z_min, z_max, eff map> tuple which corresponds to this particle, and to year that you need
    std::vector<std::tuple<double,double,TH2A*>> PID_curr_cut = (PID_eff_adaptive_inception.at(it_PIDcut->first)).at(year);
    
    //check that the boundaries are fine
    if( (var_x < std::get<2>(PID_curr_cut.front())->GetMinX())
        || (std::get<2>(PID_curr_cut.front())->GetMaxX() < var_x)
        || (var_y < std::get<2>(PID_curr_cut.front())->GetMinY())
        || (std::get<2>(PID_curr_cut.front())->GetMaxY() < var_y)
        || (nTracks < std::get<0>(PID_curr_cut.front()))
        || (std::get<1>(PID_curr_cut.back()) < nTracks)
        ){
      
      log_errors_stream << "Warning: PID kinematic values out of histo."
                        << " var_x = " << var_x
                        << " [" << std::get<2>(PID_curr_cut.front())->GetMinX()
                        << ", " << std::get<2>(PID_curr_cut.front())->GetMaxX() << "]"
                        << ", var_y = " << var_y
                        << " [" << std::get<2>(PID_curr_cut.front())->GetMinY()
                        << ", " << std::get<2>(PID_curr_cut.front())->GetMaxY() << "]" 
                        << ", ntracks = " << nTracks
                        << " [" << std::get<0>(PID_curr_cut.front())
                        << ", " << std::get<1>(PID_curr_cut.back()) << "]" 
                        << ". part_name = " << it_PIDcut->first << ", year = " << year
                        << ". Setting dummy efficiency to -1, in order to be skipped." << std::endl;
      
      SetUnvalidEfficiency(PIDEff);
      
      return;
    }  //check the boundaries
    
    //search for the correct slice on the z axis: loop over the vector elements
    for(std::vector<std::tuple<double,double,TH2A*>>::const_iterator it_slice = PID_curr_cut.begin();
        it_slice != PID_curr_cut.end(); ++it_slice){
      
      //remember:
      //get<0>: zmin, get<1>: zmax, get<2>: TH2A histo
      
      //check if nTracks is within the range of the current histo
      if((std::get<0>(*it_slice) > nTracks) || (nTracks > std::get<1>(*it_slice)))
        continue;
      
      int i_bin = (std::get<2>(*it_slice))->FindBin(var_x, var_y);
      
      //temp structure to store the data
      Efficiency PIDEff_temp;
      
      //efficiency
      PIDEff_temp.efficiency = (std::get<2>(*it_slice))->GetBinContent(i_bin);
      
      //errors
      PIDEff_temp.eff_err = (std::get<2>(*it_slice))->GetBinError(i_bin);
      PIDEff_temp.eff_err_low = (std::get<2>(*it_slice))->GetBinErrorLow(i_bin);
      PIDEff_temp.eff_err_up = (std::get<2>(*it_slice))->GetBinErrorUp(i_bin);
      
      //to understand how to correctly set them...
      PIDEff_temp.num_total = 1.;
      PIDEff_temp.num_passed = 1.;
      
      PIDEff_vect.push_back(PIDEff_temp);
      
    } //loop over the slices
  }  //loop over the cuts
  
  //now combine all the single efficiencies
  CombineEfficiencies(PIDEff, PIDEff_vect);
  
  return;
}

//----------------------------------------------
// Get single components of the kinematic efficiency
//----------------------------------------------
bool effLUT::GetKinComponents(const double Kin_1, const double Kin_2,
                              Efficiency &FiducialEff, Efficiency &GeneratorEff,
                              Efficiency &TriggerToSelEff,
                              Efficiency &TriggerEff, Efficiency &RecoEff,
                              Efficiency &SelectionEff, const std::string &year){
  
  //if it's switched off, return unvalid efficiencies
  if(!SingleKinComponents_eff_on){
    
    //SetUnvalidEfficiency(GeneratorEff);
    //SetUnvalidEfficiency(FiducialEff);
    SetUnvalidEfficiency(TriggerEff);
    //SetUnvalidEfficiency(TriggerToSelEff);
    SetUnvalidEfficiency(RecoEff);
    SetUnvalidEfficiency(SelectionEff);
    
    return true;
  }
  
  //fiducial efficiency
  if(Fiducial_Eff_on){
    if(Fiducial_eff_map.find(year) == Fiducial_eff_map.end()){
      std::cout << "GetKineEff:: Error: the selected year of the fiducial eff does not exist. year = "
                << year << std::endl;
      exit(EXIT_FAILURE);  
    }
    
    //get the fiducial efficiency
    FiducialEff = Fiducial_eff_map.at(year); 
  }
  else
    SetEfficiencyOne(FiducialEff);
  
  //check that the year exists
  if((h_generator_eff_adaptive_map.find(year) == h_generator_eff_adaptive_map.end())
     || (h_TriggerToSel_eff_adaptive_map.find(year) == h_TriggerToSel_eff_adaptive_map.end())
     || (h_trigger_eff_adaptive_map.find(year) == h_trigger_eff_adaptive_map.end())
     || (h_reco_eff_adaptive_map.find(year) == h_reco_eff_adaptive_map.end())
     || (h_selection_eff_adaptive_map.find(year) == h_selection_eff_adaptive_map.end())){
    std::cout << "GetKinComponents:: Error: the selected year does not exist. year = "
              << year << std::endl;
    exit(EXIT_FAILURE);  
  }
  
  //generator efficiency
  GetSingleEff(Kin_1, Kin_2, h_generator_eff_adaptive_map.at(year), GeneratorEff);
  
  //trigger efficiency
  GetSingleEff(Kin_1, Kin_2, h_trigger_eff_adaptive_map.at(year), TriggerEff);
  
  //reconstruction efficiency
  GetSingleEff(Kin_1, Kin_2, h_reco_eff_adaptive_map.at(year), RecoEff);
  
  //selection efficiency
  GetSingleEff(Kin_1, Kin_2, h_selection_eff_adaptive_map.at(year), SelectionEff);
  
  //trigger-->selection efficiency
  GetSingleEff(Kin_1, Kin_2, h_TriggerToSel_eff_adaptive_map.at(year), TriggerToSelEff);
  
  //now check the efficiencies
  if((!CheckValidEfficiency(GeneratorEff))
     || (!CheckValidEfficiency(TriggerEff))
     || (!CheckValidEfficiency(RecoEff))
     || (!CheckValidEfficiency(SelectionEff))
     || (!CheckValidEfficiency(TriggerToSelEff)))
    return false;
  else
    return true;
  
}

//------------
// Constructor
//------------
effLUT::effLUT(const std::string _configFileName, TFile* _outFile)
  : configFileName(_configFileName) {
  
  outFile = _outFile;
  
  //set the name of the error log stream, starting from the outFile name
  std::string log_name = outFile->GetName();
  
  boost::replace_all(log_name, ".root", "_errlog.log");
  
  std::cout << "log_name = " << log_name << std::endl;
  
  //open the output stream for the log
  log_errors_stream.open(log_name, ios::out);
  
  //write some informations in the out log
  log_errors_stream << "#log error file created by the effLUT library" << std::endl;
  log_errors_stream << "configFileName = " << _configFileName << std::endl;
  log_errors_stream << "outFileName = " << outFile->GetName() << std::endl;
  
  //-------------------------------//
  //  read the configuration file  //
  //-------------------------------//
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
  
  //set which efficiencies are enabled
  if(configtree.get_child_optional("kinEff.kinEff"))
    kin_eff_on = (bool) configtree.get<unsigned int>("kinEff.kinEff");
  else
    kin_eff_on = false;
  
  if(configtree.get_child_optional("BDTEff.BDTEff"))
    BDT_eff_on = (bool) configtree.get<unsigned int>("BDTEff.BDTEff");
  else
    BDT_eff_on = false;
  
  if(configtree.get_child_optional("PIDEff.PIDEff"))
    PID_eff_on = (bool) configtree.get<unsigned int>("PIDEff.PIDEff");
  else
    PID_eff_on = false;
  
  if(configtree.get_child_optional("kinEff.singleComp_status"))
    SingleKinComponents_eff_on = (bool) configtree.get<unsigned int>("kinEff.singleComp_status");
  else
    SingleKinComponents_eff_on = false;
  
  //some default values
  Fiducial_Eff_on = false;
  Generator_Eff_on = false;
  TriggerToSel_Eff_on = false;
  
  LoadMaps();
  
  return;
}

//---------------------------------------------------------
// To load the fiducial efficiency
//---------------------------------------------------------
Efficiency effLUT::LoadFiducialEff(std::string input_filename){
  
  input_filename = ParseEnvName(input_filename);
  
  Efficiency Fiducial_eff;
  
  //if the fiducial efficiency is not enabled, set it to 1.
  if(input_filename == ""){
    SetEfficiencyOne(Fiducial_eff);
    return Fiducial_eff;
  }
  
  std::cout << "Loading Fiducial efficiency from: " << input_filename << std::endl;
  
  //retrieve the fiducial efficiency from the .info file
  pt::ptree fiducial_file;
  pt::read_info(input_filename, fiducial_file);
  
  Fiducial_eff.efficiency = fiducial_file.get<double>("efficiency");
  
  Fiducial_eff.eff_err = fiducial_file.get<double>("error");
  Fiducial_eff.eff_err_low = fiducial_file.get<double>("error_low");
  Fiducial_eff.eff_err_up = fiducial_file.get<double>("error_up");
  
  Fiducial_eff.num_total = fiducial_file.get<double>("num_total");
  Fiducial_eff.num_passed = fiducial_file.get<double>("num_passed");
  
  return Fiducial_eff;
}

//---------------------------------------------------------
// Set the kinematic efficiencies
//---------------------------------------------------------
void effLUT::SetKinEfficiencies(){

  std::string input_filename;
  std::string input_histoname;

  TFile *input_file;
  
  //loop over the years of the kinematic efficiencies
  for(pt::ptree::const_iterator kinmaps_year = configtree.get_child("kinEff.years").begin();
      kinmaps_year != configtree.get_child("kinEff.years").end(); ++kinmaps_year)  {

    std::string year = kinmaps_year->first;
      
    //check that the current year has to be processed
    if(std::find(years.begin(), years.end(), year) == years.end())
      continue;

    //set the fiducial efficiency
    if(kinmaps_year->second.get<std::string>("inFile_Fiducial") != ""){
      Fiducial_Eff_on = true;
      
      Fiducial_eff_map.insert(std::make_pair(year,
                                             LoadFiducialEff(kinmaps_year->second.get<std::string>("inFile_Fiducial"))));
    }
    
    //set the generator efficiency
    if(kinmaps_year->second.get<std::string>("inFile_Generator") != ""){
      
      Generator_Eff_on = true;

      //get the input file name of the generator level cuts
      input_filename = ParseEnvName(kinmaps_year->second.get<std::string>("inFile_Generator"));

      //get the input histo name generator level cuts
      input_histoname = kinmaps_year->second.get<std::string>("inHistoEff_Generator");
      
      input_file = new TFile(input_filename.c_str(), "READ");

      //set the generator file and histo
      GeneratorEffFile_map.insert(std::make_pair(year, input_file));
      
      h_generator_eff_adaptive_map.insert(std::make_pair(year, SetKinLookuptable(GeneratorEffFile_map.at(year),
                                                                                 input_filename, input_histoname)));
    }
    
    //set the trigger --> sel efficiency
    if(kinmaps_year->second.get<std::string>("inFile_TriggerToSel") != ""){

      TriggerToSel_Eff_on = true;

      //get the input file name and histo name of the trigger to sel efficiency
      input_filename = ParseEnvName(kinmaps_year->second.get<std::string>("inFile_TriggerToSel"));
      input_histoname = kinmaps_year->second.get<std::string>("inHistoEff_TriggerToSel");

      input_file = new TFile(input_filename.c_str(), "READ");
      
      //set the trigger to sel file and histo
      TriggerToSelEffFile_map.insert(std::make_pair(year, input_file));
      h_TriggerToSel_eff_adaptive_map.insert(std::make_pair(year, SetKinLookuptable(TriggerToSelEffFile_map.at(year),
                                                                                    input_filename, input_histoname)));
      
    }
    
    //---------------------------------------//
    //  set the single kinematic components  //
    //---------------------------------------//

    if(SingleKinComponents_eff_on){

      //trigger component
      input_filename = ParseEnvName(kinmaps_year->second.get<std::string>("singleComp.inFile_Trigger"));
      input_histoname = kinmaps_year->second.get<std::string>("singleComp.inHisto_Trigger");

      input_file = new TFile(input_filename.c_str(), "READ");
      
      TriggerEffFile_map.insert(std::make_pair(year, input_file));
      h_trigger_eff_adaptive_map.insert(std::make_pair(year, SetKinLookuptable(TriggerEffFile_map.at(year),
                                                                               input_filename, input_histoname)));

      //reconstruction + stripping component
      input_filename = ParseEnvName(kinmaps_year->second.get<std::string>("singleComp.inFile_Reco"));
      input_histoname = kinmaps_year->second.get<std::string>("singleComp.inHisto_Reco");

      input_file = new TFile(input_filename.c_str(), "READ");
      
      RecoEffFile_map.insert(std::make_pair(year, input_file));
      h_reco_eff_adaptive_map.insert(std::make_pair(year, SetKinLookuptable(RecoEffFile_map.at(year),
                                                                            input_filename, input_histoname)));
      
      //selection
      input_filename = ParseEnvName(kinmaps_year->second.get<std::string>("singleComp.inFile_Selection"));
      input_histoname = kinmaps_year->second.get<std::string>("singleComp.inHisto_Selection");
      
      input_file = new TFile(input_filename.c_str(), "READ");
      
      SelectionEffFile_map.insert(std::make_pair(year, input_file));
      h_selection_eff_adaptive_map.insert(std::make_pair(year, SetKinLookuptable(SelectionEffFile_map.at(year),
                                                                                 input_filename, input_histoname)));
    }  //if(SingleKinComponents_eff_on)
  }  //loop over the years 
    
  return;
}


//---------------------------------------------------------
// Set the lookup table for the kinematic efficiency
//---------------------------------------------------------
TH2A* effLUT::SetKinLookuptable(TFile *inEffFile,
                                std::string input_filename, std::string inHisto_name){
  
  input_filename = ParseEnvName(input_filename);
  
  inEffFile = TFile::Open(input_filename.c_str(), "READ");
  
  std::cout << "Loading efficiency histo from: " << input_filename << std::endl;
  
  if(inEffFile == NULL){
    std::cout<< "Error: the efficiency file " << input_filename << " does not exist." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //open histo with efficiency
  return (TH2A*) inEffFile->Get(inHisto_name.c_str());
}
      
//---------------------------------------------------------
// Set the PID efficiencies
//---------------------------------------------------------
void effLUT::SetPIDEfficiencies(){
  
  //loop over the PID cuts
  for(pt::ptree::const_iterator PID_cut = configtree.get_child("PIDEff.PIDCuts").begin();
      PID_cut != configtree.get_child("PIDEff.PIDCuts").end(); ++PID_cut){
    
    //temp structure, to store all the years of the current particle
    std::map<std::string, std::vector<std::tuple<double,double,TH2A*>>> PID_particle_allyears;
  
    //loop over the years
    for(pt::ptree::const_iterator PID_year = PID_cut->second.get_child("years").begin();
        PID_year != PID_cut->second.get_child("years").end(); ++PID_year){
      
      //check that the current year has to be processed
      if(std::find(years.begin(), years.end(), PID_year->first) == years.end())
        continue;
        
      std::string input_filename = ParseEnvName(PID_year->second.get<std::string>("inFile"));
      
      TFile *pidEffFile = TFile::Open(input_filename.c_str(), "READ");
      
      if(pidEffFile == NULL){
        std::cout << "Error: PID efficiency file " << input_filename << " does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      std::cout << "Loading PID efficiency histo from: " << input_filename << std::endl;
      
      //load the different adaptive histos, and store them in a tuple
      TVectorD *zBins_vect = (TVectorD *) pidEffFile->Get((PID_year->second.get<std::string>("inHistoEff") + "_numZbins").c_str());
      
      if(zBins_vect == NULL){
        std::cout << "Error: the vector with the number of zBins for the PID adaptive does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }
      
      int numZbins = (*zBins_vect)[0];
      
      std::vector<std::tuple<double,double,TH2A*>> PID_eff_adaptive_vector_temp;
      
      //loop over input histos                                                                                                            
      for(int i_Zbin = 1; i_Zbin <= numZbins; i_Zbin++){
        
        std::string histo_name = PID_year->second.get<std::string>("inHistoEff") + "_" + std::to_string(i_Zbin);
        
        TH2A *PID_eff_adaptive_curr = (TH2A*) ((TH2A*) pidEffFile->Get(histo_name.c_str()))->Clone();
        
        if(PID_eff_adaptive_curr == NULL){
          std::cout << "Error: the PID adaptive histo " << histo_name << std::endl;
          exit(EXIT_FAILURE);
        }
        
        //now I set the boundaries of the nTracks slices
        std::string zRange_name = PID_year->second.get<std::string>("inHistoEff") + "_" +std::to_string(i_Zbin) +"_zRanges";
        
        TVectorD *zRanges = (TVectorD *) pidEffFile->Get(zRange_name.c_str());
        
        if(zRanges == NULL){
          std::cout << "Error: the vector with zRanges " << zRange_name << " for the PID adaptive does not exist." << std::endl;
          exit(EXIT_FAILURE);
        }
        
        PID_eff_adaptive_vector_temp.push_back(std::make_tuple((*zRanges)[0], (*zRanges)[1],
                                                               PID_eff_adaptive_curr));
        
      }  //loop over input histos                                                                                                                                           
      
      //a check of the loaded vector
      for(std::vector<std::tuple<double,double,TH2A*>>::const_iterator it_vect = PID_eff_adaptive_vector_temp.begin();
          it_vect != PID_eff_adaptive_vector_temp.end(); ++it_vect)
        std::cout << "nTracks min = " << std::get<0>(*it_vect) << ", max = " << std::get<1>(*it_vect) << std::endl;
      
      //add the current year to the PID cut of the current particle
      PID_particle_allyears.insert(std::make_pair(PID_year->first,
                                                  PID_eff_adaptive_vector_temp));
      
      //close the input file
      pidEffFile->Close();
      
    }  //loop over the years

    //add the current particle to the mega-structure
    PID_eff_adaptive_inception.insert(std::make_pair(PID_cut->second.get<std::string>("part_name"),
                                                     PID_particle_allyears));
    
  }  //loop over the PID cuts
  
  return;
}

//---------------------------------------------------------
// Set the BDT efficiencies
//---------------------------------------------------------
void effLUT::SetBDTEfficiencies(){

  //loop over the BDT cuts
  for(pt::ptree::const_iterator BDT_cut = configtree.get_child("BDTEff.BDTCuts").begin();
      BDT_cut != configtree.get_child("BDTEff.BDTCuts").end(); ++BDT_cut){

    //temp structure, to store all the years of the current particle
    std::map<std::string, TH2A*> BDT_particle_allyears;
    
    //loop over the years
    for(pt::ptree::const_iterator BDT_year = BDT_cut->second.get_child("years").begin();
        BDT_year != BDT_cut->second.get_child("years").end(); ++BDT_year){

      //check that the current year has to be processed
      if(std::find(years.begin(), years.end(), BDT_year->first) == years.end())
        continue;

      //open the input file
      std::string input_filename = ParseEnvName(BDT_year->second.get<std::string>("inFile"));

      TFile *BDTEffFile = TFile::Open(input_filename.c_str(), "READ");
      
      if(BDTEffFile == NULL){
        std::cout << "Error: BDT efficiency file " << input_filename << " does not exist." << std::endl;
        exit(EXIT_FAILURE);
      }

      std::cout << "Loading BDT efficiency histo from: " << input_filename << std::endl;

      //get the eff histo
      std::string histo_name = BDT_year->second.get<std::string>("inHisto");
	
      TH2A *BDT_eff_adaptive_histo = (TH2A*) ((TH2A*) BDTEffFile->Get(histo_name.c_str()))->Clone();

      if(BDT_eff_adaptive_histo == NULL){
        std::cout << "Error: the BDT adaptive histo " << histo_name << std::endl;
        exit(EXIT_FAILURE);
      }

      //add the current year to the BDT cut of the current particle
      BDT_particle_allyears.insert(std::make_pair(BDT_year->first,
                                                  BDT_eff_adaptive_histo));
      
      //close the input file
      BDTEffFile->Close();
      
    }  //loop over the years
    
    //add the current particle to the mega-structure
    BDT_eff_adaptive_inception.insert(std::make_pair(BDT_cut->second.get<std::string>("part_name"),
                                                     BDT_particle_allyears));
  }  //loop over the BDT cuts
  
  return;
}
  
//---------------------------------------------------------
// To load TH2A maps
//---------------------------------------------------------
void effLUT::LoadMaps(){
  
  //------------------//
  //  load the years  //
  //------------------//

  //possible years
  std::vector<std::string> possible_years = {"2011", "2012", "2015", "2016", "2017", "2018"};
  
  //loop over the possible years, to see if they are enabled
  for(std::vector<std::string>::const_iterator it_year = possible_years.begin();
      it_year != possible_years.end(); ++it_year){
    
    if(configtree.get_child_optional("status_years." + (*it_year)))
      if(configtree.get<int>("status_years." + (*it_year)) == 1){
        //update the vector of years
        years.push_back(*it_year);
        
        std::cout << "selected year = " << *it_year << std::endl;
      }
  }  //loop over the possible years
  
  //-----------------------------------//
  //  load the kinematic efficiencies  //
  //-----------------------------------//
  
  if(kin_eff_on)
    SetKinEfficiencies();
    
  //-----------------------------//
  //  load the BDT efficiencies  //
  //-----------------------------//
  
  if(BDT_eff_on)
    SetBDTEfficiencies();
  
  //-----------------------------//                                                                                                                                                         
  //  load the PID efficiencies  //                                                                                     
  //-----------------------------//

  if(PID_eff_on)
    SetPIDEfficiencies();
  
  return;
  
}


//-----------
// Destructor
//-----------
effLUT::~effLUT(){

  /*
  //close the input files
  std::map<int, TFile*>::iterator it;
  
  it = GeneratorEffFile_map.begin();
  
  while(it != GeneratorEffFile_map.end()){
    if(it->second != NULL)
      it->second->Close();
  }
  
  it = TriggerToSelEffFile_map.begin();
  
  while(it != TriggerToSelEffFile_map.end()){
    if(it->second != NULL)
      it->second->Close();
  }
  */
}
