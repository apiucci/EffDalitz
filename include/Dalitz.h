#ifndef INCLUDE_DALITZ_H 
#define INCLUDE_DALITZ_H 1

/** @class Dalitz Dalitz.h include/Dalitz.h
 *  
 *  @author  Alessio Piucci
 *  @date    18-03-2016
 *  @brief   A class to handle Dalitz plots.
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser
#include <utility>   //to use std::pair
#include <fstream>   //to write the log in an external stream

#include "commonLib.h"

//ROOT libraries
#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TEntryList.h>
#include <TLorentzVector.h>
#include <TEfficiency.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>

using namespace std;
namespace pt = boost::property_tree;

class Dalitz {
public: 

  /// Standard constructor
  Dalitz(const pt::ptree _configtree, std::string _inFileName, TFile* _outFile); 

  ~Dalitz( ); ///< Destructor
  
  //make a Dalit Plot
  TH2D* MakeDalitzPlot(TTree *inTree, const bool generated_ForEff,
                       unsigned int &num_events);
  
protected:

private:

  //input file name
  std::string inFileName;
  
  //output file
  TFile *outFile;
  
  //configuration Boost tree
  pt::ptree configtree;
  
};
#endif // INCLUDE_DALITZ_H
