/*!
 *  @file      Dalitz.cpp
 *  @author    Alessio Piucci
 *  @brief     A class to handle Dalitz plots.
 */

#include "Dalitz.h"

//------------
// Constructor
//------------
Dalitz::Dalitz(const pt::ptree _configtree, std::string _inFileName, TFile* _outFile)
  : configtree(_configtree) {

  inFileName = _inFileName;
  
  configtree = _configtree;

  outFile = _outFile;
  
}

//----------
// Destructor
//-----------
Dalitz::~Dalitz(){
  
}

//-------------------
// Make a Dalitz plot
//-------------------
TH2D* Dalitz::MakeDalitzPlot(TTree *inTree, const bool generated_ForEff, unsigned int &num_events){
  
  TH2D* h_Dalitz = new TH2D("h_Dalitz",
                            (configtree.get<std::string>("DalitzPlot.title")).c_str(),
                            configtree.get<unsigned int>("DalitzPlot.syst1.num_bins"),     //number of bins
                            configtree.get<double>("DalitzPlot.syst1.low_mass"),           //plot range
                            configtree.get<double>("DalitzPlot.syst1.high_mass"),
                            configtree.get<unsigned int>("DalitzPlot.syst2.num_bins"),     //number of bins
                            configtree.get<double>("DalitzPlot.syst2.low_mass"),           //plot range
                            configtree.get<double>("DalitzPlot.syst2.high_mass"));
  
  TH1D* h_invmass_1 = new TH1D("h_invmass_1",
                               (configtree.get<std::string>("DalitzPlot.syst1.title")).c_str(),
                               configtree.get<unsigned int>("DalitzPlot.syst1.num_bins"),     //number of bins
                               configtree.get<double>("DalitzPlot.syst1.low_mass"),            //plot range
                               configtree.get<double>("DalitzPlot.syst1.high_mass"));
  
  TH1D* h_invmass_2 = new TH1D("h_invmass_2",
                               (configtree.get<std::string>("DalitzPlot.syst2.title")).c_str(),
                               configtree.get<unsigned int>("DalitzPlot.syst2.num_bins"),     //number of bins
                               configtree.get<double>("DalitzPlot.syst2.low_mass"),            //plot range
                               configtree.get<double>("DalitzPlot.syst2.high_mass"));
  
  TH1D* h_invmass_mother = new TH1D("h_invmass_mother",
                                    (configtree.get<std::string>("options.mother_plot.title")).c_str(),
                                    configtree.get<unsigned int>("options.mother_plot.num_bins"),      //number of bins
                                    configtree.get<double>("options.mother_plot.mass_low"),      //plot range
                                    configtree.get<double>("options.mother_plot.mass_high"));
  
  //parse the cuts, for reconstructed of generated candidates
  std::string cuts_string = ParseCuts(configtree, generated_ForEff);
  
  //I use a TEntryList to select only candidates which pass the cuts
  inTree->Draw(">>list_mothers", cuts_string.c_str(), "entrylist");
  
  TEntryList *list_mothers = (TEntryList*) gDirectory->Get("list_mothers");
  
  if(generated_ForEff)
    std::cout << "--> Generated tree" << std::endl;
  else
    std::cout << "--> Reconstructed tree" << std::endl;
  
  std::cout << "cuts = " << cuts_string << std::endl;
  std::cout << "inTree->GetEntries() = " << inTree->GetEntries()
            << ", list_mothers->GetN() = " << list_mothers->GetN() << std::endl;
  std::cout << std::endl;
  
  //variable used for the normalization of histograms
  num_events = list_mothers->GetN();
  
  //--------------------------------------------------------------//
  //  setting values for the computation of the invariant masses  //
  //--------------------------------------------------------------//
  
  //pair of particle names and 4-momenta: it's assumed that 3 particles are used to make a Dalitz plot
  std::pair <std::string, TLorentzVector> particles[3];
  
  //retrieving the names of the particles
  particles[0].first = configtree.get<std::string>("options.particles.name_1");
  particles[1].first = configtree.get<std::string>("options.particles.name_2");
  particles[2].first = configtree.get<std::string>("options.particles.name_3");
  
  //names of kinematic variables
  std::string kin_vars_name[4];
  
  //retrieving the kinematic variable names
  kin_vars_name[0] = configtree.get<std::string>("options.kinvar_names.px");
  kin_vars_name[1] = configtree.get<std::string>("options.kinvar_names.py");
  kin_vars_name[2] = configtree.get<std::string>("options.kinvar_names.pz");
  kin_vars_name[3] = configtree.get<std::string>("options.kinvar_names.E");
  
  //values of the kinematic variables for the three particles
  //[px, py, pz, E]
  double kin_vars_value[3][4];
  
  //loop over the 3 particles
  for(unsigned int i_part = 0; i_part < 3; ++i_part){
    
    //filling kinematic variables from the input tree
    for(unsigned int i_kinvar = 0; i_kinvar < 4; ++i_kinvar)
      inTree->SetBranchAddress((particles[i_part].first + kin_vars_name[i_kinvar]).c_str(),  //name of the variable
                               kin_vars_value[i_part] + i_kinvar);  //variable to fill
    
  }  //loop over particles
 
  //mapping the invariant mass systems (2 systems = 4 particles) with the 3 available particles
  int particles_syst1[2] = {-1};
  int particles_syst2[2] = {-1};
  
  //loop over the 3 particle names
  for(unsigned int i_part = 0; i_part < 3; ++i_part){
    if(particles[i_part].first == configtree.get<std::string>("DalitzPlot.syst1.part1"))
      particles_syst1[0] = i_part;
    
    if(particles[i_part].first == configtree.get<std::string>("DalitzPlot.syst1.part2"))
      particles_syst1[1] = i_part;
    
    if(particles[i_part].first == configtree.get<std::string>("DalitzPlot.syst2.part1"))
      particles_syst2[0] = i_part;
    
    if(particles[i_part].first == configtree.get<std::string>("DalitzPlot.syst2.part2"))
      particles_syst2[1] = i_part;
  }  //loop over particle names
 
  //--------------//
  // reweighting  //
  //--------------//
  
  double weight_2Ddistr;
  
  TH2D *h_weights = new TH2D();
  
  double x_var_value;
  double y_var_value;
  
  //reweight reconstructed !and generated! candidates, based on a 2D histo
  if((bool) configtree.get<unsigned int>("reweight.2DReweight.reweight")){
    
    //open the input file with the weights
    TFile *weights_file = TFile::Open((configtree.get<std::string>("reweight.2DReweight.file_name")).c_str(), "READ");
  
    //retrieve 2D histo with weights
    h_weights = (TH2D*) weights_file->Get((configtree.get<std::string>("reweight.2DReweight.name")).c_str());
    
    //names of the variables used for the 2D weight histo
    std::string x_var_name = configtree.get<std::string>("reweight.2DReweight.x_var");
    std::string y_var_name = configtree.get<std::string>("reweight.2DReweight.y_var");
    
    //read x, y values from the input tree
    inTree->SetBranchAddress(x_var_name.c_str(), &x_var_value);
    inTree->SetBranchAddress(y_var_name.c_str(), &y_var_value);
    
    outFile->cd();
    h_weights->Write("2DReweight_weights");
  }  //if((bool) configtree.get<unsigned int>("efficiency.2DReweight"))
  
  
  //-------------------------------//
  //  loop over mother candidates  //
  //-------------------------------//
                               
  //loop over all mother candidates, and fill the Dalitz plot with the related weights
  for(unsigned int i_cand = 0; i_cand < num_events; ++i_cand){
    
    inTree->GetEntry(list_mothers->GetEntry(i_cand));
    
    //fill the 4-momenta from the kinematic variables,
    //loop over the 3 particles
    for(unsigned int i_part = 0; i_part < 3; ++i_part){
      particles[i_part].second.SetPxPyPzE(kin_vars_value[i_part][0],
                                          kin_vars_value[i_part][1],
                                          kin_vars_value[i_part][2],
                                          kin_vars_value[i_part][3]);
    }  //loop over particles
    
    /////
    
    //should I reweight?
    if((bool) configtree.get<unsigned int>("reweight.2DReweight.reweight")){
      weight_2Ddistr = h_weights->Interpolate(x_var_value, y_var_value);
    
      //if(weight_2Ddistr == 0)
      //  std::cout << "Warning: weight 0 for (x, y) = (" << x_var_value << ", "
      //            << y_var_value << ")." << std::endl;
    }
    else
      weight_2Ddistr = 1.;
    
    /////
    
    //fill 1-D projections and mother mass
    h_invmass_1->Fill((particles[particles_syst1[0]].second + particles[particles_syst1[1]].second).M2(),
                      weight_2Ddistr);
    h_invmass_2->Fill((particles[particles_syst2[0]].second + particles[particles_syst2[1]].second).M2(),
                      weight_2Ddistr);
    
    h_invmass_mother->Fill((particles[0].second + particles[1].second + particles[2].second).M());
    
    //finally fill the Dalitz plot
    h_Dalitz->Fill((particles[particles_syst1[0]].second + particles[particles_syst1[1]].second).M2(),
                   (particles[particles_syst2[0]].second + particles[particles_syst2[1]].second).M2(),
                   weight_2Ddistr);
    
  }  //loop over mothers candidates
  
  //set some style options for the Dalitz plot
  h_Dalitz->GetXaxis()->SetTitle((configtree.get<std::string>("DalitzPlot.syst1.axis_label")).c_str());
  h_Dalitz->GetYaxis()->SetTitle((configtree.get<std::string>("DalitzPlot.syst2.axis_label")).c_str());
  h_Dalitz->GetXaxis()->SetTitleOffset(1.2);
  h_Dalitz->GetYaxis()->SetTitleOffset(1.3);
  
  h_invmass_1->GetXaxis()->SetTitle((configtree.get<std::string>("DalitzPlot.syst1.axis_label")).c_str());
  h_invmass_1->GetYaxis()->SetTitle("entries");
  h_invmass_1->GetXaxis()->SetTitleOffset(1.2);
  h_invmass_1->GetYaxis()->SetTitleOffset(1.3);
  
  h_invmass_2->GetYaxis()->SetTitle((configtree.get<std::string>("DalitzPlot.syst2.axis_label")).c_str());
  h_invmass_2->GetYaxis()->SetTitle("entries");
  h_invmass_2->GetXaxis()->SetTitleOffset(1.2);
  h_invmass_2->GetYaxis()->SetTitleOffset(1.3);
  
  outFile->cd();
  
  //write the plots the output file
  if(!generated_ForEff){
    h_Dalitz->SetName((configtree.get<std::string>("options.out_name") + "_reco").c_str());
    h_Dalitz->Write(h_Dalitz->GetName());
    
    h_invmass_1->Write(("h1_" + configtree.get<std::string>("DalitzPlot.syst1.title") + "_reco").c_str());
    h_invmass_2->Write(("h1_" + configtree.get<std::string>("DalitzPlot.syst2.title") + "_reco").c_str());
    h_invmass_mother->Write((configtree.get<std::string>("options.mother_plot.out_name") + "_reco").c_str());
  }
  else
  {
    h_Dalitz->SetName((configtree.get<std::string>("options.out_name") + "_gen").c_str());
    h_Dalitz->Write(h_Dalitz->GetName());
    
    h_invmass_1->Write(("h1_" + configtree.get<std::string>("DalitzPlot.syst1.title") + "_gen").c_str());
    h_invmass_2->Write(("h1_" + configtree.get<std::string>("DalitzPlot.syst2.title") + "_gen").c_str());
    h_invmass_mother->Write((configtree.get<std::string>("options.mother_plot.out_name") + "_gen").c_str());
  }
  
  //memory cleaning
  delete h_invmass_1;
  delete h_invmass_2;
  delete h_invmass_mother;
  
  return h_Dalitz;
}

