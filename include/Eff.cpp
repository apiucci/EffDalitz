/*!
 *  @file      Eff.cpp
 *  @author    Alessio Piucci
 *  @brief     A class which provides basic tools to compute efficiencies.
 */

#include "Eff.h"

//------------
// Constructor
//------------
Eff::Eff(const pt::ptree _configtree, std::string _inFileName, std::string _outFileName)
  : configtree(_configtree) {

  inFileName = _inFileName;
  
  configtree = _configtree;
  
  outFileName = _outFileName;
  
  min_events = 0;
  
  //retrieve the path and name of the out log
  std::string out_root_name = ParseEnvName(outFileName + ".root");
  std::string log_name = ParseEnvName(outFileName + ".log");
  
  std::cout << "out_root_name = " << out_root_name << std::endl;
  std::cout << "log_file = " << log_name << std::endl;
  
  //open the output TFile
  outFile = new TFile(out_root_name.c_str(), "recreate");
  
  //open the output stream for the log
  log_stream.open(log_name, ios::out);
  
  //write some informations in the out log
  log_stream << "#logfile created by the Eff library" << std::endl;
  log_stream << "inFileName = " << inFileName << std::endl;
  log_stream << "outFileName = " << outFile->GetName() << std::endl;
  
}

//----------
// Destructor
//-----------
Eff::~Eff(){
  
}

//-----------------------------------------------------------------------------
// Retrieve and parse the normalisation value from an external file
//-----------------------------------------------------------------------------
double Eff::NormalisationParsing(std::string frac){
  
  //is the normalisation factor to apply?
  if(configtree.get_child_optional("normalise.Normalise" + frac)){
    if((bool) configtree.get<unsigned int>(("normalise.Normalise" + frac))){
      
      //------------------------------------//
      //  retrieve the normalisation value  //
      //------------------------------------//
      
      //create empty property tree object
      pt::ptree configtree_norm;
      
      //parse the INFO into the property tree.
      pt::read_info(configtree.get<std::string>("normalise.Normalise" + frac + "_inFile"), configtree_norm);
      
      return configtree_norm.get<double>("GenLevCutsEff.genlevcut.eff");
      
    }  //if normalisation required
  }
  
  return 1.;
  
}

//-----------------------------------------------------------------------------
//  Normalisation of denominator and denominator
//-----------------------------------------------------------------------------
void Eff::NormaliseSample(TH2D* h_2D_reco){
  
  //----------------------------------------------//
  //  do we need of renormalise the two samples,  //
  //  used as numerator and denominator?          //
  //----------------------------------------------//
  
  //scaling factor, to correctly compute the efficiency for the out log
  double scaling = 1.;

  if(configtree.get_child_optional("normalise.NormaliseNum")){
    if((bool) configtree.get<unsigned int>(("normalise.NormaliseNum"))
       || (bool) configtree.get<unsigned int>(("normalise.NormalieDen"))){
      double NumNorm = NormalisationParsing("Num");
      double DenNorm = NormalisationParsing("Den");
      
      //for simplicity, I only normalise the numerator
      double requiredNumEvents = ((double) num_entries_genSample)*(NumNorm/DenNorm);
      
      scaling = requiredNumEvents/ (double) num_entries_recoSample;
      
      std::cout << "NORMALISATION: NumNorm = " << NumNorm << ", DenNorm = " << DenNorm
		<< ", requiredNumEvents = " << requiredNumEvents  << std::endl;
      std::cout << "num_gen_events = " << num_entries_genSample << std::endl;
      std::cout << "num_reco_events = " << num_entries_recoSample << std::endl;
      std::cout << "scaling = " << scaling << std::endl;
      std::cout << std::endl;
      
      h_2D_reco->Scale(scaling);
      
      //save the scaled histo
      outFile->cd();
      
      h_2D_reco->Write((configtree.get<std::string>("numerator.out_name") + "_reco_scaled").c_str());
      
      log_stream << "NORMALISESAMPLE:: num_gen_events = " << num_entries_genSample << std::endl;
      log_stream << "NORMALISESAMPLE:: num_reco_events = " << num_entries_recoSample << std::endl;
      log_stream << "NORMALISESAMPLE:: reconstructed histo rescaled by the factor = " << scaling << std::endl;
      
    }  //if normalisation required
  }
  
  return;
  
}

//-----------------------------------------------------------------------------
// Compute the mean efficiency value for TH2D objects, simple or weighted by the number of generated events in the bins
//-----------------------------------------------------------------------------
double Eff::MeanEff(TH2D *h_eff, TH2D *h_gen, bool weighted){
  
  //------------------------------------------------------//
  //  to take in account empty bins with efficiency = 0,  //
  //  I first loop over the bins of the generated histo.  //
  //  If the generated bin is empty, I skip the related   //
  //  bin of the efficiency histo                         //
  //------------------------------------------------------//
  
  //I compute the mean efficiency, weighted by the number of generated events in the bins
  double average = 0.;
  double num_generated;
  double weight;
  double weights_sum = 0.;
  
  //loop over the bins of the generated plot
  for(int i_bin = 0; i_bin < h_gen->GetNcells(); ++i_bin){
    
    num_generated = h_gen->GetBinContent(i_bin);
    
    //check if there's something generated
    if(num_generated > 0){
      
      //weighted by the number of generated events in the bin?
      if(weighted)
        weight = num_generated;
      else
        weight = 1.;
      
      //now I retrieve the efficiency value from the eff histo
      average += (h_eff->GetBinContent(i_bin) * weight);
      
      //weighted by the number of generated events in the bin?
      weights_sum += weight;
    
    }  //if(num_generated > 0)
  }  //loop over the bins of the generated plot
  
  //compute the average
  average /= weights_sum;
  
  return average;
}

//-----------------------------------------------------------------------------
// Compute the mean efficiency value for TEfficiency objects, simple or weighted by the number of generated events in the bins
//-----------------------------------------------------------------------------
double Eff::MeanEff(TEfficiency *h_eff, bool weighted){
  
  //------------------------------------------------------//
  //  to take in account empty bins with efficiency = 0,  //
  //  I first loop over the bins of the generated histo.  //
  //  If the generated bin is empty, I skip the related   //
  //  bin of the efficiency histo                         //
  //------------------------------------------------------//
  
  //I compute the mean efficiency, weighted by the number of generated events in the bins
  double average = 0.;
  double num_generated;
  double weight;
  double weights_sum = 0.;
  
  //loop over the bins of the generated plot
  for(int i_bin = 0; i_bin < h_eff->GetTotalHistogram()->GetNcells(); ++i_bin){
    
    num_generated = h_eff->GetTotalHistogram()->GetBinContent(i_bin);

    //check if there's something generated
    if(num_generated > 0){
      
      //weighted by the number of generated events in the bin? 
      if(weighted)
        weight = num_generated;
      else
        weight = 1.;
      
      //now I retrieve the efficiency value from the eff histo
      average += (h_eff->GetEfficiency(i_bin) * weight);
      
      //weighted by the number of generated events in the bin?
      weights_sum += weight;
      
    }  //if(num_generated > 0)
  }  //loop over the bins of the generated plot
  
  //compute the average
  average /= weights_sum;
  
  return average;
}

//-------------------
// Make a 2D plot
//-------------------
TH2D* Eff::Make2DPlot(TTree *inTree, const bool generated_ForEff, unsigned int &num_events){
  
  TH2D* h_2D = new TH2D("h_2D",
                        (configtree.get<std::string>("numerator.title")).c_str(),
                        configtree.get<unsigned int>("numerator.var1_numbins"),   //number of bins
                        configtree.get<double>("numerator.var1_low"),             //plot range
                        configtree.get<double>("numerator.var1_high"),
                        configtree.get<unsigned int>("numerator.var2_numbins"),   //number of bins
                        configtree.get<double>("numerator.var2_low"),             //plot range
                        configtree.get<double>("numerator.var2_high"));
  
  TH1D* h_var_1 = new TH1D("h_var_1",
                           (configtree.get<std::string>("numerator.var1_title")).c_str(),
                           configtree.get<unsigned int>("numerator.var1_numbins"), //number of bins
                           configtree.get<double>("numerator.var1_low"),           //plot range
                           configtree.get<double>("numerator.var1_high"));
  
  TH1D* h_var_2 = new TH1D("h_var_2",
                           (configtree.get<std::string>("numerator.var2_title")).c_str(),
                           configtree.get<unsigned int>("numerator.var2_numbins"),  //number of bins
                           configtree.get<double>("numerator.var2_low"),            //plot range
                           configtree.get<double>("numerator.var2_high"));
  
  
  //parse the cuts, for reconstructed of generated candidates
  std::string cuts_string = ParseCuts(configtree, generated_ForEff);


  //--------------------------------//
  // get weights from (friend) tree //
  //--------------------------------//

  double weight;
    
  TFile* fFriendTree = 0;
  TTree* tFriendTree = 0;
  bool reweightFromTree = false;
  bool reweightGen = false;

  if(configtree.get_child_optional("reweight.fromTree.reweightGen"))
    reweightGen = configtree.get<bool>("reweight.fromTree.reweightGen");
  
  if(configtree.get_child_optional("reweight.fromTree")){
    if(configtree.get<bool>("reweight.fromTree.reweightFromTree")){
      reweightFromTree = true;
      if(configtree.get<bool>("reweight.fromTree.useFriendTree")){
        TString fFriendTreeName;
        if(!generated_ForEff) // reco tree
          fFriendTreeName = configtree.get<std::string>("reweight.fromTree.friendFileReco");
        else if (reweightGen)
          fFriendTreeName = configtree.get<std::string>("reweight.fromTree.friendFileGen");
        if (!generated_ForEff || reweightGen) {
          fFriendTree = TFile::Open(fFriendTreeName,"READ");
          if(fFriendTree == NULL){
            std::cout << "Error: Missing file for friend tree containing weights: " << fFriendTreeName << std::endl;
            exit(EXIT_FAILURE); 
          }
        }
        if(!generated_ForEff) // reco tree
          tFriendTree = (TTree*) fFriendTree->Get((configtree.get<std::string>("reweight.fromTree.friendTreeReco")).c_str());
        else if (reweightGen)
          tFriendTree = (TTree*) fFriendTree->Get((configtree.get<std::string>("reweight.fromTree.friendTreeGen")).c_str());
        if (!generated_ForEff || reweightGen) {
          if(tFriendTree == NULL){
            std::cout << "Error: Missing friend tree containing weights." << std::endl;
            exit(EXIT_FAILURE); 
          }
          inTree->AddFriend(tFriendTree);
        }
      }
      if(!generated_ForEff) // reco tree
        inTree->SetBranchAddress((configtree.get<std::string>("reweight.fromTree.weightNameReco")).c_str(), &weight);
      else if (reweightGen)
        inTree->SetBranchAddress((configtree.get<std::string>("reweight.fromTree.weightNameGen")).c_str(), &weight);

      if(configtree.get_child_optional("reweight.2DReweight.reweight"))
	if((bool) configtree.get<unsigned int>("reweight.2DReweight.reweight"))
	  cout << "Warning: Using weights from tree. 2D weights histo specified in config file has no effect" << endl;

    }
  }

  
  //I use a TEntryList to select only candidates which pass the cuts
  inTree->Draw(">>list_events", cuts_string.c_str(), "entrylist");
  
  TEntryList *list_events = (TEntryList*) gDirectory->Get("list_events");
  
  if(generated_ForEff)
    std::cout << "--> Generated tree" << std::endl;
  else
    std::cout << "--> Reconstructed tree" << std::endl;
  
  std::cout << "cuts = " << cuts_string << std::endl;
  std::cout << "inTree->GetEntries() = " << inTree->GetEntries()
            << ", list_events->GetN() = " << list_events->GetN() << std::endl;
  std::cout << std::endl;
  
  //variable used for the normalisation of the histos
  if(generated_ForEff)
    num_entries_genSample = inTree->GetEntries();
  else
    num_entries_recoSample = inTree->GetEntries();
  
  num_events = list_events->GetN();
  
  //some printouts in the output log file
  if(generated_ForEff)
    log_stream << "GENERATED" << std::endl;
  else
    log_stream << "RECO" << std::endl;
    
  log_stream << "--> cuts = " << cuts_string << std::endl;
  log_stream << "--> events before cuts = " << inTree->GetEntries() << std::endl;
  log_stream << "--> events after cuts = " << num_events << std::endl;
  
  //----------------------------------------//
  //  retrieve the variables from the tree  //
  //----------------------------------------//
  
  //values of the variables
  double var_1, var_2;
  
  //names of the variables, for reconstructed or generated case
  std::string var_1_name, var_2_name;
  
  if(!generated_ForEff){
    var_1_name = configtree.get<std::string>("numerator.var1_name");
    var_2_name = configtree.get<std::string>("numerator.var2_name");
  }
  else
  {
    var_1_name = configtree.get<std::string>("denominator.var1_name");
    var_2_name = configtree.get<std::string>("denominator.var2_name");
  }
  
  //set the addresses of the variables
  inTree->SetBranchAddress(var_1_name.c_str(), &var_1);
  inTree->SetBranchAddress(var_2_name.c_str(), &var_2);
  
  
  //--------------//
  // reweighting  //
  //--------------//
    
  TH2D *h_weights = new TH2D();
  
  double x_var_value;
  double y_var_value;
  
  //reweight reconstructed candidates, based on a 2D histo
  if(configtree.get_child_optional("reweight.2DReweight.reweight")){
    if((bool) configtree.get<unsigned int>("reweight.2DReweight.reweight")){
      
      //open the input file with the weights
      TFile *weights_file = TFile::Open((configtree.get<std::string>("reweight.2DReweight.file_name")).c_str(), "READ");
      
      //retrieve 2D histo with weights
      h_weights = (TH2D*) weights_file->Get((configtree.get<std::string>("reweight.2DReweight.name")).c_str());
      
      //names of the variables used for the 2D weight histo
      std::string x_var_name = configtree.get<std::string>("reweight.2DReweight.x_var");
      std::string y_var_name = configtree.get<std::string>("reweight.2DReweight.y_var");
      
      //read x, y values from the input tree
      inTree->SetBranchAddress(x_var_name.c_str(), &x_var_value);
      inTree->SetBranchAddress(y_var_name.c_str(), &y_var_value);
      
      outFile->cd();
      h_weights->Write("2DReweight_weights");
    }  //if((bool) configtree.get<unsigned int>("denominator.2DReweight"))
  }

  //--------------------//
  //  loop over events  //
  //--------------------//

  bool reweight2D = false;
  
  if(configtree.get_child_optional("reweight.2DReweight.reweight"))
    reweight2D = (bool) configtree.get<unsigned int>("reweight.2DReweight.reweight");

  //loop over all events, and fill plot with the related weights
  for(unsigned int i_cand = 0; i_cand < num_events; ++i_cand){
    
    inTree->GetEntry(list_events->GetEntry(i_cand));
    
    //should I reweight using 2D histo? only if not using weights from tree
    if(!reweightFromTree && (bool) reweight2D){
      weight = h_weights->Interpolate(x_var_value, y_var_value);

      //if(weight == 0)
      //  std::cout << "Warning: weight 0 for (x, y) = (" << x_var_value << ", "
      //            << y_var_value << ")." << std::endl;
    }
    else if (!reweightFromTree || (!reweightGen && generated_ForEff)){
      weight = 1.;
    }
    // if none of the above cases apply, weight = weight from
    // (friend) tree
      
    //fill 1-D projections
    h_var_1->Fill(var_1);
    h_var_2->Fill(var_2);
    
    //finally fill the 2D plot
    h_2D->Fill(var_1, var_2, weight);
        
  }  //loop over mothers candidates

  //set some style options for the 2D plot
  h_2D->GetXaxis()->SetTitle((configtree.get<std::string>("numerator.x_axis_label")).c_str());
  h_2D->GetYaxis()->SetTitle((configtree.get<std::string>("numerator.y_axis_label")).c_str());
  h_2D->GetXaxis()->SetTitleOffset(1.2);
  h_2D->GetYaxis()->SetTitleOffset(1.3);
  
  h_var_1->GetXaxis()->SetTitle((configtree.get<std::string>("numerator.x_axis_label")).c_str());
  h_var_1->GetYaxis()->SetTitle("entries");
  h_var_1->GetXaxis()->SetTitleOffset(1.2);
  h_var_1->GetYaxis()->SetTitleOffset(1.3);
  
  h_var_2->GetYaxis()->SetTitle((configtree.get<std::string>("numerator.y_axis_label")).c_str());
  h_var_2->GetYaxis()->SetTitle("entries");
  h_var_2->GetXaxis()->SetTitleOffset(1.2);
  h_var_2->GetYaxis()->SetTitleOffset(1.3);
  
  outFile->cd();
  
  //write the plots the output file
  if(!generated_ForEff){
    h_2D->SetName((configtree.get<std::string>("numerator.out_name") + "_reco").c_str());
    h_2D->Write(h_2D->GetName());
    h_var_1->Write(("h1_" + configtree.get<std::string>("numerator.var1_title") + "_reco").c_str());
    h_var_2->Write(("h1_" + configtree.get<std::string>("numerator.var2_title") + "_reco").c_str());
  }
  else
  {
    h_2D->SetName((configtree.get<std::string>("numerator.out_name") + "_gen").c_str());
    h_2D->Write(h_2D->GetName());
    h_var_1->Write(("h1_" + configtree.get<std::string>("numerator.var1_title") + "_gen").c_str());
    h_var_2->Write(("h1_" + configtree.get<std::string>("numerator.var2_title") + "_gen").c_str());
  }
  
  //memory cleaning
  delete h_var_1;
  delete h_var_2;
  
  return h_2D;
}

//--------------------------------------//
// Compute errors for TH2A with weights //
//--------------------------------------//
void Eff::ComputeErrorsWithWeights_adaptive(TH2D* hGen, TH2A hGenAda, TH2D* hReco, TH2A hRecoAda, TH2A& hEffAda){
  // Compute errors with effective number of entries per
  // reco/gen bin: N_eff = (sum_i w_i)^2/(sum_i w_i^2) instead
  // of N = sum_i w_i. This is done by first obtaining w_i and
  // w_i^2 from the fine-grained TH2D per bin and then summing
  // these quantities per TH2A bin.

  // Gen
  int binsGenAda = hGenAda.GetNBins();
  int binGenAda;
  double sumw_Gen[binsGenAda+1] = {};
  double sumw2_Gen[binsGenAda+1] = {};
  double N_eff_Gen[binsGenAda+1] = {};

  hGen->Sumw2();
  TArrayD* arr_Sumw2_Gen = (TArrayD*) hGen->GetSumw2();
  Double_t binCenterXGen;
  Double_t binCenterYGen;
  Int_t globalBinGen;

  for (int i=1; i<=hGen->GetXaxis()->GetNbins(); i++) {
    for (int j=1; j<=hGen->GetYaxis()->GetNbins(); j++) {
      binCenterXGen = hGen->GetXaxis()->GetBinCenter(i);
      binCenterYGen = hGen->GetYaxis()->GetBinCenter(j);
      binGenAda = hGenAda.FindBin(binCenterXGen, binCenterYGen);
      globalBinGen = hGen->FindBin(binCenterXGen, binCenterYGen);
      sumw_Gen[binGenAda]+=hGen->GetBinContent(globalBinGen);
      sumw2_Gen[binGenAda]+=arr_Sumw2_Gen->GetAt(globalBinGen);
    }
  }
  for (int i=1; i<=binsGenAda; i++) {
    N_eff_Gen[i] = sumw_Gen[i]*sumw_Gen[i]/sumw2_Gen[i];
    //cout << "Correction factor sum w/sum w^2 for gen bin " << i << ": " << sumw_Gen[i]/sumw2_Gen[i] << endl;
  }


  // Reco
  int binsRecoAda = hRecoAda.GetNBins();
  int binRecoAda;
  double sumw_Reco[binsRecoAda+1] = {};
  double sumw2_Reco[binsRecoAda+1] = {};
  double N_eff_Reco[binsRecoAda+1] = {};

  hReco->Sumw2();
  TArrayD* arr_Sumw2_Reco = (TArrayD*) hReco->GetSumw2();
  Double_t binCenterXReco;
  Double_t binCenterYReco;
  Int_t globalBinReco;

  for (int i=1; i<=hReco->GetXaxis()->GetNbins(); i++) {
    for (int j=1; j<=hReco->GetYaxis()->GetNbins(); j++) {
      binCenterXReco = hReco->GetXaxis()->GetBinCenter(i);
      binCenterYReco = hReco->GetYaxis()->GetBinCenter(j);
      binRecoAda = hRecoAda.FindBin(binCenterXReco, binCenterYReco);
      globalBinReco = hReco->FindBin(binCenterXReco, binCenterYReco);
      sumw_Reco[binRecoAda]+=hReco->GetBinContent(globalBinReco);
      sumw2_Reco[binRecoAda]+=arr_Sumw2_Reco->GetAt(globalBinReco);
    }
  }
  for (int i=1; i<=binsRecoAda; i++) {
    N_eff_Reco[i] = sumw_Reco[i]*sumw_Reco[i]/sumw2_Reco[i];
    //cout << "Correction factor sum w/sum w^2 for reco bin " << i << ": " << sumw_Reco[i]/sumw2_Reco[i] << endl;
  }
  

  double m_ConfidenceLevel = 0.6827;
  for (int i=1; i<=binsGenAda; i++) {
    double conf_inv_lo = TEfficiency::Wilson(N_eff_Gen[i],N_eff_Reco[i],m_ConfidenceLevel,false);
    double conf_inv_hi = TEfficiency::Wilson(N_eff_Gen[i],N_eff_Reco[i],m_ConfidenceLevel,true);
    double content = N_eff_Reco[i]/N_eff_Gen[i];
    hEffAda.SetBinErrorLow(i,content-conf_inv_lo);
    hEffAda.SetBinErrorUp(i,conf_inv_hi-content);
    if (std::isnan(conf_inv_lo) || std::isnan(conf_inv_hi)) {
      std::cout << "Warning: error of bin " << i << " is nan! Setting it to 0." << std::endl;
      hEffAda.SetBinErrorLow(i,0.d);
      hEffAda.SetBinErrorUp(i,0.d);
    }
  }

  return;

}

//----------------------------------------
// Compute the efficiency on a 2D plot, with adaptive binning
//----------------------------------------
void Eff::Compute2DEfficiency_adaptive(TH2D* h_2D_gen, TH2D* h_2D_reco){
  
  //normalisation of numerator and denominator
  NormaliseSample(h_2D_reco);
  
  TH2A h_gen_adaptive;
  TH2A h_reco_adaptive;
  
  //check if we need to recompute the binning
  std::string binningName = ParseEnvName(outFileName + "_binning.adb");
  std::ifstream testBinning(binningName);
  
  if(!testBinning.good()){
    //we have to recompute the binning
  
    std::cout << "No binning scheme was found. Creating a new one here: " << binningName << std::endl;
    
    //check if the min_events parameter it's correctly set
    if(min_events <= 0){
      std::cout << "Error: min_events is not set. Exiting. "<< std::endl;
      exit(EXIT_FAILURE);  
    }
    
    //fill the adaptive binning histos for generated
    h_gen_adaptive.MakeBinning(std::vector<TH2D>{*h_2D_gen, *h_2D_reco}, min_events);
    
    //write the binning
    h_gen_adaptive.WriteBinning(binningName);
    
  }
  else
  {
    std::cout << "Found a binning scheme, loading it from: " << binningName << std::endl;
    
    //we already have some binning scheme, just fill the histo with the generated
    h_gen_adaptive = TH2A(binningName);
    h_gen_adaptive.FillFromHist(*h_2D_gen);
    
  }
  
  //fill the reconstructed histo
  h_reco_adaptive = h_gen_adaptive;
  h_reco_adaptive.FillFromHist(*h_2D_reco);
  
  //for debug
  //h_gen_adaptive.SetVerbosity(3);
  //h_reco_adaptive.SetVerbosity(3);
  
  //compute the efficiency!
  TH2A h_eff_adaptive;
  
  //h_eff_adaptive.SetVerbosity(3);
  
  h_eff_adaptive.SetStatOpt(TEfficiency::kFWilson);
  h_eff_adaptive.BinomialDivide(h_reco_adaptive, h_gen_adaptive);

  //set the axis titles
  h_eff_adaptive.SetXaxisTitle(configtree.get<std::string>("numerator.x_axis_label"));
  h_eff_adaptive.SetYaxisTitle(configtree.get<std::string>("numerator.y_axis_label"));
  
  //for debug
  //h_gen_adaptive.Dump();
  //h_reco_adaptive.Dump();
  //h_eff_adaptive.Dump();


  // recalculate errors if weights are used
  bool reweightFromTree = false;
  bool reweight2D = false;

  if(configtree.get_child_optional("reweight.fromTree.reweightFromTree"))
     reweightFromTree = configtree.get<bool>("reweight.fromTree.reweightFromTree");
     
  if(configtree.get_child_optional("reweight.2DReweight.reweight"))
    reweight2D = configtree.get<bool>("reweight.2DReweight.reweight");
  
  if (reweightFromTree || reweight2D)
    ComputeErrorsWithWeights_adaptive(h_2D_gen, h_gen_adaptive, h_2D_reco, h_reco_adaptive, h_eff_adaptive);
  
  //save into the .root output file
  outFile->cd();
  
  h_2D_gen->Write("h_TH2D_gen");
  h_2D_reco->Write("h_TH2D_reco");
  
  h_gen_adaptive.Write("h_gen_adaptive");
  h_reco_adaptive.Write("h_reco_adaptive");
  
  h_eff_adaptive.Write((configtree.get<std::string>("numerator.out_name") + "_eff").c_str());
  
  //compute the average efficiency, weighted by the generated distribution
  double average_eff = GetAverageEfficiency_adaptive(h_gen_adaptive, h_eff_adaptive);
  
  
  //---------------------------------//
  //  efficiency as simple division  //
  //---------------------------------//
  
  TH2D *h_2D_SimpleEff = (TH2D*) h_2D_reco->Clone("h_2D_SimpleEff");
  
  h_2D_SimpleEff->Reset();
  h_2D_SimpleEff->Divide(h_2D_reco, h_2D_gen, 1., 1., "B");
  
  outFile->cd();
  h_2D_SimpleEff->Write((configtree.get<std::string>("numerator.out_name") + "_SimpleEff").c_str());
  
  //write some infos in the output log
  log_stream << "TH2D:: h_gen->GetEntries() = " << h_2D_gen->GetEntries()
             << ", h_gen->Integral() = " << h_2D_gen->Integral() << std::endl;
  log_stream << "TH2D:: h_reco->GetEntries() = " << h_2D_reco->GetEntries()
             << ", h_reco->Integral() = " << h_2D_reco->Integral() << std::endl;
  log_stream << "TH2D:: simple average efficiency (from simple TH2D division) = "
             << MeanEff(h_2D_SimpleEff, h_2D_gen, false) << std::endl;
  log_stream << "TH2D:: average efficiency (from simple TH2D division), weighted by the generated distribution = "
             << MeanEff(h_2D_SimpleEff, h_2D_gen, true) << std::endl;
  
  log_stream << "ADAPTIVE:: average_eff, weighted by the generated distribution = " << average_eff << std::endl;
  
  std::cout << "ADAPTIVE:: average_eff, weighted by the generated distribution = " << average_eff << std::endl;
  
  //save the important statistics in the output info file
  double low_CL = TEfficiency::Wilson(h_2D_gen->GetEntries(), h_2D_reco->GetEntries(), 0.6832, false);
  double up_CL = TEfficiency::Wilson(h_2D_gen->GetEntries(), h_2D_reco->GetEntries(), 0.6832, true);
  
  double efficiency = h_2D_reco->GetEntries() / h_2D_gen->GetEntries();
  
  double err = (up_CL - low_CL)/2.;
  double err_low = efficiency - low_CL;
  double err_up = up_CL - efficiency;
  
  info_stream.put<double>("efficiency", efficiency);
  info_stream.put<double>("error", err);
  info_stream.put<double>("error_up", err_up);
  info_stream.put<double>("error_low", err_low);
  info_stream.put<double>("num_passed", h_2D_reco->GetEntries());
  info_stream.put<double>("num_total", h_2D_gen->GetEntries());

  std::ofstream out_tex;
  out_tex.open(outFileName + ".tex");
  //loop over nodes of the out tree

  std::vector<std::string> more_stuff;
  boost::split(more_stuff, outFileName, boost::is_any_of("/"));
  auto TeX_prefix = more_stuff.back();

  vector<pair<string,string>> texrepl =
  {{"0","Zero"},{"1","One"},{"2","Two"},{"3","Three"},{"4","Four"},{"5","Five"},{"6","Six"},{"7","Seven"},{"8","Eight"},{"9","Nine"}};
  if(any_of(TeX_prefix.begin(),TeX_prefix.end(),[](const auto& c){return !isalpha(c);})){
    for(const auto& r : texrepl)
      boost::algorithm::replace_all(TeX_prefix,r.first,r.second);
    TeX_prefix.erase(remove_if(TeX_prefix.begin(),TeX_prefix.end(),[](const auto& c){return !isalpha(c);}),TeX_prefix.end());
  }
  out_tex << "\\def \\" << TeX_prefix << "WMean {" << h_eff_adaptive.GetWMean() << "}" << endl;
  out_tex << "\\def \\" << TeX_prefix << "dWMean {" << h_eff_adaptive.GetdWMean() << "}" << endl;
  
  return;
}

//----------------------------------------
// Compute the efficiency on a 2D plot
//----------------------------------------
void Eff::Compute2DEfficiency(TH2D* h_2D_gen, TH2D* h_2D_reco,
                              TH2D* h_2D_errors_gen, TH2D* h_2D_errors_reco){
                                              
  //normalisation of numerator and denominator
  NormaliseSample(h_2D_reco);
  
  //the TEfficiency doesn't like not empty underflow and overflow bins
  h_2D_gen->ClearUnderflowAndOverflow();
  h_2D_reco->ClearUnderflowAndOverflow();
  
  //number of bins which were fixed, to use the TEfficiency
  unsigned int num_fixed_bins = 0;
  
  //to use the same axis ranges for the error histo, I clone the original histo and then reset its content
  h_2D_errors_gen = (TH2D*) h_2D_gen->Clone("h_2D_errors_gen");
  h_2D_errors_gen->Reset();
  
  h_2D_errors_reco = (TH2D*) h_2D_reco->Clone("h_2D_errors_reco");
  h_2D_errors_reco->Reset();
  
  
  //---------------------------------//
  //  efficiency as simple division  //
  //---------------------------------//
  
  TH2D *h_2D_SimpleEff = (TH2D*) h_2D_reco->Clone("h_2D_SimpleEff");
  h_2D_SimpleEff->Reset();
  
  h_2D_SimpleEff->Divide(h_2D_reco, h_2D_gen, 1., 1., "B");
  
  outFile->cd();
  h_2D_SimpleEff->Write((configtree.get<std::string>("numerator.out_name") + "_SimpleEff").c_str());
  
  
  //--------------------------------//
  //  efficiency using TEfficiency  //
  //--------------------------------//
  
  //check consistency of generated and reco histograms
  if(!TEfficiency::CheckConsistency(*h_2D_reco, *h_2D_gen)){
    std::cout << "Warning: reconstructed and generated histos are not consistent!"
              << " Proceeding to fix the problematic entries." << std::endl;
    FixTEfficiencyEntries(h_2D_reco, h_2D_gen, h_2D_errors_reco, h_2D_errors_gen, num_fixed_bins);
  }
  
  log_stream << "num_fixed_bins = " << num_fixed_bins 
             << " (" << (num_fixed_bins/(double) h_2D_reco->GetNcells())*100 << " %)" << std::endl;
  
  TEfficiency *h_2D_eff = new TEfficiency(*h_2D_reco, *h_2D_gen);
  
  //use Wilson statistic, please don't ask me but use this reference:
  //https://root.cern.ch/doc/master/classTEfficiency.html
  h_2D_eff->SetConfidenceLevel(0.683);
  h_2D_eff->SetStatisticOption(TEfficiency::kFWilson);
  
  //set titles
  std::string title = configtree.get<std::string>("denominator.title");
  std::string x_title = configtree.get<std::string>("numerator.x_axis_label");
  std::string y_title = configtree.get<std::string>("numerator.y_axis_label");
  std::string title_complete = title + ";" + x_title + ";" + y_title;
  
  h_2D_eff->SetTitle(title_complete.c_str());
  
  //check that everything went fine
  if( !CheckEfficiency(h_2D_eff)){
    std::cout << "Fatal error: the created TEfficiency is not valid!"
              << "This should never be, since the gen and reco histos should be already checked and fixed."<< std::endl;
    exit(EXIT_FAILURE);
  }
  
  //saving into the .root output file
  outFile->cd();
  h_2D_eff->Write((configtree.get<std::string>("numerator.out_name") + "_eff").c_str());
  
  //write the average efficiency in the out log
  log_stream << "TH2D:: h_gen->GetEntries() = " << h_2D_gen->GetEntries()
             << ", h_gen->Integral() = " << h_2D_gen->Integral() << std::endl;
  log_stream << "TH2D:: h_reco->GetEntries() = " << h_2D_reco->GetEntries()
             << ", h_reco->Integral() = " << h_2D_reco->Integral() << std::endl;
  log_stream << "TH2D:: simple average efficiency (from simple TH2D division) = "
             << MeanEff(h_2D_SimpleEff, h_2D_gen, false) << std::endl;
  log_stream << "TH2D:: average efficiency (from simple TH2D division), weighted by the generated distribution = "
             << MeanEff(h_2D_SimpleEff, h_2D_gen, true) << std::endl;
  
  log_stream << "TEFFICIENCY:: simple average efficiency = " << MeanEff(h_2D_eff, false) << std::endl;
  log_stream << "TEFFICIENCY:: average efficiency, weighted by the generated distribution = "
             << MeanEff(h_2D_eff, true) << std::endl;
  
  std::cout << "TEFFICIENCY: average efficiency, weighted by the generated distribution = "
            << MeanEff(h_2D_eff, true) << std::endl;
  
  //save the important statistics in the output info file
  double low_CL = TEfficiency::Wilson(h_2D_gen->GetEntries(), h_2D_reco->GetEntries(), 0.6832, false);
  double up_CL = TEfficiency::Wilson(h_2D_gen->GetEntries(), h_2D_reco->GetEntries(), 0.6832, true);
  
  double efficiency = h_2D_reco->GetEntries() / h_2D_gen->GetEntries();
  
  double err = (up_CL - low_CL)/2.;
  double err_low = efficiency - low_CL;
  double err_up = up_CL - efficiency;
  
  info_stream.put<double>("efficiency", efficiency);
  info_stream.put<double>("error", err);
  info_stream.put<double>("error_up", err_up);
  info_stream.put<double>("error_low", err_low);
  info_stream.put<double>("num_passed", h_2D_reco->GetEntries());
  info_stream.put<double>("num_total", h_2D_gen->GetEntries());
  
  //cleaning of memory
  delete h_2D_SimpleEff;
  delete h_2D_eff;
  
  return;
}

//----------------------------------------
//  Compute the average efficiency value, weighted by the content of another histo
//----------------------------------------
double Eff::GetAverageEfficiency_adaptive(TH2A &h_weights, TH2A &h_eff){
  
  std::cout << "--> GetAverageEfficiency_adaptive" << std::endl;
  
  //first, check that the two histos have same binning
  if(! h_weights.HasSameBinning(h_eff)){
    std::cout << "Error: the two histos have different binning." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //average efficiency, weighted
  double average_eff = 0;
  
  //sum of the weights
  double sum_weights = 0;
  
  //loop over bins
  for(unsigned int i_bin = 1; i_bin <= h_weights.GetNBins(); ++i_bin){

    //std::cout << "h_eff.GetBinContent(i_bin) = " << h_eff.GetBinContent(i_bin)
    //          << ", h_weights.GetBinContent(i_bin) = " << h_weights.GetBinContent(i_bin) << std::endl;
    
    if((h_eff.GetBinContent(i_bin) < 0) || (h_eff.GetBinContent(i_bin) > 1)
       || std::isnan(h_eff.GetBinContent(i_bin)))
      continue;
    
    average_eff += h_eff.GetBinContent(i_bin) * h_weights.GetBinContent(i_bin);
    sum_weights += h_weights.GetBinContent(i_bin);
  }
  
  average_eff /= sum_weights;
  
  return average_eff;
}

//----------------------------------------
// Finalize the job, writing the output files
//----------------------------------------
void Eff::Finalize(){
  
  outFile->Write();
  outFile->Close();
  
  log_stream.close();
  
  write_info(ParseEnvName(outFileName + ".info"), info_stream);

  return;
}

