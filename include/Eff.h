#ifndef INCLUDE_EFF_H
#define INCLUDE_EFF_H 1

/** @class Eff Eff.h include/Eff.h
 *  
 *
 *  @author  Alessio Piucci
 *  @date    12-07-2016
 *  @brief   A class which provides basic tools to compute efficiencies.
 */

// Include files
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser
#include <utility>   //to use std::pair
#include <fstream>   //to write the log in an external stream

#include "commonLib.h"
#include "../2DAdaptiveBinning/include/TH2A.h"

//ROOT libraries
#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TEntryList.h>
#include <TLorentzVector.h>
#include <TEfficiency.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
namespace pt = boost::property_tree;

class Eff {
public: 

  /// Standard constructor
  Eff(const pt::ptree _configtree, std::string _inFileName, std::string _outFileName);

  ~Eff( ); ///< Destructor
  
  //retrieve and parse the normalisation value from an external file
  double NormalisationParsing(std::string frac);
  
  //normalisation of denominator and denominator
  void NormaliseSample(TH2D* h_2D_reco);
  
  //make a 2D Plot
  TH2D* Make2DPlot(TTree *inTree, const bool generated_ForEff, unsigned int &num_events);

  //compute errors for TH2A with weights
  void ComputeErrorsWithWeights_adaptive(TH2D* hGen, TH2A hGenAda, TH2D* hReco, TH2A hRecoAda, TH2A& hEffAda);
  
  //compute the efficiency on a 2D plot, with adaptive binning
  void Compute2DEfficiency_adaptive(TH2D* h_2D_gen, TH2D* h_2D_reco);
  
  //compute the efficiency on a 2D plot
  void Compute2DEfficiency(TH2D* h_2D_gen, TH2D* h_2D_reco,
                           TH2D* h_2D_errors_gen, TH2D* h_2D_errors_reco);
  
  //compute the mean efficiency value for TH2D objects, simple or weighted by the number of generated events in the bins
  double MeanEff(TH2D *h_eff, TH2D *h_gen, bool weighted);
  
  //compute the mean efficiency value for TEfficiency objects, simple or weighted by the number of generated events in the bins
  double MeanEff(TEfficiency *h_eff, bool weighted);
  
  //compute the average efficiency value, weighted by the content of another histo
  double GetAverageEfficiency_adaptive(TH2A &h_weights, TH2A &h_eff);
  
  //finalize the job, writing the output files
  void Finalize();

  //set the minimum number of events per bin, for adaptive plots
  void SetMinEvents(unsigned int _min_events){ min_events = _min_events; };
  
  //get the output TFile
  TFile* GetOutTFile(){ return outFile;};

  //set the gen and reco entries for the rescaling 
  void SetGenRecoEntries(unsigned int gen_entries, unsigned int reco_entries){
    num_entries_genSample = gen_entries;
    num_entries_recoSample = reco_entries;
    return;
  };
  
protected:

private:

  //input and output file name
  std::string inFileName;
  std::string outFileName;
  
  //output file
  TFile *outFile;
  
  //configuration Boost tree
  pt::ptree configtree;

  //output log and info streams
  std::ofstream log_stream;
  pt::ptree info_stream;
  
  //minimum number of events per bin, for adaptive plots
  unsigned int min_events;
  
  //number of events in the gen and reco samples,
  //used for rescaling of the samples
  unsigned int num_entries_genSample;
  unsigned int num_entries_recoSample;
  
};
#endif // INCLUDE_EFF_H
