
# check which input code files are modified in the repo
# usage: call gitchecked(<list_of_inputdatafiles>,<list_of_code_in_git>,<list_of_outputs>) in the input field of a rule
def gitchecked(inputdata,code,outputdata) :
    # the snakemake rule expects a function
    # we are not going to use the wildcards
    def checker(wildcards) :
        updatedCode=[]
        from gitcheck import updatedRepo
        for cfile in code :
           for dfile in outputdata :
             # check if code is younger than outputdata
             if updatedRepo(cfile.format(wildcards=wildcards),dfile.format(wildcards=wildcards)) : updatedCode.append(cfile.format(wildcards=wildcards))
        return updatedCode+[d.format(wildcards=wildcards) for d in inputdata]
    return checker


#######
# set the work directory
#######

# set the working directory
# to make cohexisting the branching ratio and amplitude analysis,
# I'm doing a kind of hack
import os

if ("CI_WORKDIR" in os.environ) :
   work_dir = os.environ.get("CI_WORKDIR")
else :
   work_dir = "/tmp/LcD0Kwork_EffDalitz"

os.environ["CI_WORKDIR"] = work_dir

######

# set some stuff required to compile,
# and create some of the subdirectories which are not automatically created during run-time
onstart:
   shell("if [ ! -d build ]; then mkdir build; cd build; export CC=gcc; export CXX=g++; else cd build; fi;\
          cmake ../; make -j 4; cd ..;\
          if [ ! -d `$CI_WORKDIR` ]; then mkdir $CI_WORKDIR ; fi")

#######
# define the test rules
#######

rule test_rules:
    input:
       work_dir + "/Eff2D_adaptive_test.root",
       work_dir + "/Eff1D_test.root",
       #work_dir + "/Eff2D_adaptive_filtered_test.root",
       work_dir + "/PIDEff_test.root",
       work_dir + "/effLUT_test_log.info",
       work_dir + "/mean2DEff_test.info"

rule test_make2DEff_adaptive:
    input:
      gitchecked([],
                 ["src/make2DEff_adaptive.cpp",
                  "config/config_2DEff.info"],
                 [work_dir + "/Eff2D_adaptive_test.root"])
    output:
      work_dir + "/Eff2D_adaptive_test.root",
    log:
      work_dir + "/Eff2D_adaptive_test.log",
    shell:
      "build/bin/make2DEff_adaptive -i root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/MC/Lb2LcD0K/NoRadiativeD0/MC_KinEff/MC_2012_standard_Lb2LcD0K_PIDGenResampled.root \
         -c config/config_2DEff.info -b 100 -o $CI_WORKDIR/Eff2D_adaptive_test | tee 2>&1 {log}"

rule test_make2DEff:
    input:
      gitchecked([],
                 ["src/make2DEff.cpp",
                  "config/config_2DEff.info"],
                 [work_dir + "/Eff2D_test.root"])
    output:
      work_dir + "/Eff2D_test.root",
    log:
      work_dir + "/Eff2D_test.log",
    shell:
      "build/bin/make2DEff -i root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/MC/Lb2LcD0K/NoRadiativeD0/MC_KinEff/MC_2012_standard_Lb2LcD0K_PIDGenResampled.root \
             -c config/config_2DEff.info -o $CI_WORKDIR/Eff2D_test | tee 2>&1 {log}"
	     
rule test_make1DEff:
    input:
      gitchecked([],
                 ["src/make1DEff.cpp",
                  "config/config_1DEff.info"],
                 [work_dir + "/Eff1D_test.root"])
    output:
      work_dir + "/Eff1D_test.root",
    log:
      work_dir + "/Eff1D_test.log",
    shell:
      "build/bin/make1DEff -n root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/MC/Lb2LcD0K/NoRadiativeD0/MC_KinEff/MC_2012_standard_Lb2LcD0K_PIDGenResampled.root \
             -l DecayTree_fixed_PIDresampled_renamed \
             -d root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/MC/Lb2LcD0K/NoRadiativeD0/MC_KinEff/MC_2012_standard_Lb2LcD0K_PIDGenResampled.root \
             -m MCDecayTree_fixed_2 -c config/config_1DEff.info -o $CI_WORKDIR/Eff1D_test.root | tee 2>&1 {log}"

rule test_make2DEff_adaptive_filtered:
    input:
      gitchecked([],
                 ["src/make2DEff_adaptive_filtered.cpp",
                  "config/config_2DEff_filtered.info"],
                 [work_dir + "/Eff2D_adaptive_filtered_test.root"])
    output:
      work_dir + "/Eff2D_adaptive_filtered_test.root",
    log:
      work_dir + "/Eff2D_adaptive_filtered_test.log",
    shell:
      "build/bin/make2DEff_adaptive_filtered -i root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/MC/Lb2LcD0K/NoRadiativeD0/MC_KinEff/MC_2012_standard_Lb2LcD0K_PIDGenResampled.root \
         -c config/config_2DEff_filtered.info -b 100 -o $CI_WORKDIR/Eff2D_adaptive_filtered_test | tee 2>&1 {log}"
	 
rule test_PIDEff_adaptive:
     input:
       gitchecked([],
                  ["src/makePIDEff.cpp"],
                  [work_dir + "/PIDEff_test.root"])
     output:
       work_dir + "/PIDEff_test.root"
     log:
       work_dir + "/PIDEff_test.log"
     shell:
       'build/bin/makePIDEff -i root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/PID/lookuptables_adaptive/PerfHists_K_MC12TuneV2ProbNNK_0-18_Strip20_2012_Custom_scheme_P_ETA_nTracks_adaptive_Uraniav7r0_total.root \
              -o $CI_WORKDIR/PIDEff_test -p "PassedHist_K_MC12TuneV2_ProbNNK > 0.18_All__K_P_K_Eta_nTracks" \
	      -t "TotalHist_K_MC12TuneV2_ProbNNK > 0.18_All__K_P_K_Eta_nTracks" \
              -a 1 -r 0 -1 20000 -2 60000 -3 40000 -4 10000 -5 2000 | tee 2>&1 {log}'

rule test_effLUT:
     input:
       gitchecked([],
                  ["src/yield.cpp",
		   "config/effLUT.info"],
                  [work_dir + "/effLUT_test_log.info",
		   work_dir + "/effLUT_test_tree.root"])
     output:
       work_dir + "/effLUT_test_log.info",
       work_dir + "/effLUT_test_tree.root"
     log:
       work_dir + "/effLUT_test.log"
     run:
       # copy the some eff files from EOS to the workdir
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/LcD0K_fitresults.info $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/LcD0K_fixshapes.info $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/Eff_Fiducial_signal__2011.info $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/Eff_Fiducial_signal__2012.info $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/MCDalitz_signal_GenLevCuts_DefaultBinning_2011.root $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/MCDalitz_signal_GenLevCuts_DefaultBinning_2012.root $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/MCDalitz_signal_TriggerRecoStrippingSelection_DefaultBinning_2011.root $CI_WORKDIR/")
       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/MCDalitz_signal_TriggerRecoStrippingSelection_DefaultBinning_2012.root $CI_WORKDIR/")

       shell("xrdcp -N -f root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/EffDalitz_tests/LcD0K_frtree.root $CI_WORKDIR/")
       
       # run Forrest, run!
       shell("build/bin/yield -i $CI_WORKDIR/LcD0K_frtree.root \
            -t fitted_data -c config/effLUT.info -o $CI_WORKDIR/effLUT_test | tee 2>&1 {log}")

rule test_mean2DEff:
    input:
      gitchecked([],
                 ["src/mean2DEff.cpp",
                  "config/config_2DEff.info"],
                 [work_dir + "/mean2DEff_test.info"])
    output:
      work_dir + "/mean2DEff_test.info",
    log:
      work_dir + "/mean2DEff_test.log",
    shell:
      "build/bin/mean2DEff -i root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/MC/Lb2LcD0K/NoRadiativeD0/MC_KinEff/MC_2012_standard_Lb2LcD0K_PIDGenResampled.root \
             -c config/config_2DEff.info -o $CI_WORKDIR/mean2DEff_test.info | tee 2>&1 {log}"
