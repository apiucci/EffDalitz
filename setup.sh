#!/bin/bash
############################################################################
#Setup the environment for the EffDalitz package
#USAGE: source setup.sh [<LCG-version>] [<platform>]
#       <LCG-version> defaults to 94
#       <platform>    defaults to x86_64-slc6-gcc8-opt
############################################################################

printf "\033[1;37m INFO   \033[0m: Setting up environment for the EffDalitz package\n"

#setup TH2A directory before LCG
CURR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export TH2A_DIR=$CURR_DIR/2DAdaptiveBinning
printf "\033[1;37m INFO   \033[0m: Using TH2A from $TH2A_DIR\n"

#parse lcg version
if [ -z "$1" ];then
  printf "\033[1;37m INFO   \033[0m: No LCG version specified. Defaulting to 94\n"
  LCG_VER="94"
else
  LCG_VER="$1"
fi

#parse platform
if [ -z "$2" ];then
  printf "\033[1;37m INFO   \033[0m: No platform specified. Defaulting to x86_64-slc6-gcc8-opt\n"
  PLTF="x86_64-slc6-gcc8-opt"
else
  PLTF="$2"
fi

SFT_DIR=/cvmfs/sft.cern.ch/lcg/views/LCG_$LCG_VER/$PLTF
LHCB_DIR=/cvmfs/lhcb.cern.ch/lib/lcg/releases

if [ -d "$SFT_DIR" ]; then
  printf "\033[1;37m INFO   \033[0m: Setting up environment from LCG $LCG_VER views in $SFT_DIR\n"
  source $SFT_DIR/setup.sh
  printf "\033[1;32m SUCCESS\033[0m: LCG environment was set up\n"
elif [ -d "$LHCB_DIR" ] && [ -z "$1" ]; then
  printf "\033[1;37m INFO   \033[0m: $SFT_DIR not found. Setting up environment from $LHCB_DIR\n"
  printf "\033[1;33m WARNING\033[0m: Can not parse lcg version and platform when setting up from $LHCB_DIR. The default is LCG 94 on x86_64-slc6-gcc8-opt\n"
  printf "\033[1;37m INFO   \033[0m: For a different environment, change the setup script or setup the environment by hand. Good luck...\n"
  #the gcc setup in the lhcb environment doesnt find the correct binutils. set it by hand...
  export PATH=$LHCB_DIR/binutils/2.28/x86_64-slc6/bin/:$PATH
  export LD_LIBRARY_PATH=$LHCB_DIR/binutils/2.28/x86_64-slc6/lib/:$LD_LIBRARY_PATH
  #missing libstdc++.so.6 ...
  export LD_LIBRARY_PATH=$LHCB_DIR/LCG_94/gcc/8.1.0/x86_64-slc6/lib64/:$LD_LIBRARY_PATH
  #now setup the compiler without binutils. note that lcg views could in principle do the job, but we dont have them everywhere
  source $LHCB_DIR/LCG_94/gcc/8.1.0/x86_64-slc6/setup.sh
  source $LHCB_DIR/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc8-opt/bin/thisroot.sh
  export BOOST_ROOT=$LHCB_DIR/LCG_94/Boost/1.66.0/x86_64-slc6-gcc8-opt/
  #needed by TMVA
  export LD_LIBRARY_PATH=$LHCB_DIR/LCG_94/blas/0.2.20.openblas/x86_64-slc6-gcc8-opt/lib64/:$LD_LIBRARY_PATH
  #needed by other ROOT libraries
  export LD_LIBRARY_PATH=$LHCB_DIR/GSL/2.5-32fc5/x86_64-slc6-gcc8-opt/lib/:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=$LHCB_DIR/fftw3/3.3.4-a8420/x86_64-slc6-gcc8-opt/lib/:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=$LHCB_DIR/LCG_94/xrootd/4.8.4/x86_64-slc6-gcc8-opt/lib64/:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=$LHCB_DIR/LCG_94/png/1.6.17/x86_64-slc6-gcc8-opt/lib/:$LD_LIBRARY_PATH
  export LD_LIBRARY_PATH=$LHCB_DIR/LCG_94/tbb/2018_U1/x86_64-slc6-gcc8-opt/lib/:$LD_LIBRARY_PATH
  printf "\033[1;32m SUCCESS\033[0m: LCG environment was set up\n"
else
  printf "\033[1;31m ERROR  \033[0m: Can not setup environment. Make sure cvmfs is mounted..."
fi
