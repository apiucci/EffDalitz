# function to compare the timestamp of a file (tagfile) to the date last modification of another file in git (repofile)

import git
import os.path, datetime, sys
import pytz

# the function has to be executed in the root of the repository and
# repofile path should be relative to that root
# return true if repofile is younger than tagfile (or tagfile does not exist)
# else return false
def updatedRepo(repofile, tagfile) :
    #print("Checking time of last modification of " + repofile + " against timestamp of " + tagfile,file=sys.stderr)
    # get last time of modification
    ctrepo=""
    try :
        if 'externals' in repofile :
            elements=repofile.split('/')
            externrepo="externals/{}/".format(elements[1])
            repofile=repofile.split(externrepo)[1]
            #print("Checking external package {} for file {}".format(externrepo,repofile))
            r = git.Repo(externrepo)
        else :
            r = git.Repo('.')
        ctrepo=r.git.log('-1','--format=%cd',repofile)
        if ctrepo=="" :
            # check for external dependency
            #print("Checking change of external packages")
            if "externals/" in repofile : ctrepo=r.git.log('-1','--format=%cd','externals/')
        trepo=datetime.datetime.strptime(ctrepo, "%a %b %d %H:%M:%S %Y %z")
        # convert to utc
        utc = pytz.utc
        trepoutc=trepo.astimezone(utc)
        #print(trepoutc,file=sys.stderr)
    except ValueError :
        print("Checked time of last modification of " + repofile + " against timestamp of " + tagfile,file=sys.stderr)
        print("time data of {} does not match given format.\nwill treat exception as if repo was updated".format(repofile),file=sys.stderr)
        print(ctrepo)
        return True
    
    # check if tagfile exists
    # get timestamp and make it timezone aware
    try :
        ttag=datetime.datetime.utcfromtimestamp(os.path.getmtime(tagfile)).replace(tzinfo=utc)
        #print(ttag,file=sys.stderr)
    except FileNotFoundError :
        print("Checked time of last modification of " + repofile + " against timestamp of " + tagfile,file=sys.stderr)
        print("{} not found".format(tagfile),file=sys.stderr)
        # in this case the repofile is younger
        return True

    timedif=ttag-trepo
    #print(timedif,file=sys.stderr)

    if trepo>ttag :
        print("Checked time of last modification of " + repofile + " against timestamp of " + tagfile,file=sys.stderr)
        print("Repofile younger than tagfile",file=sys.stderr)
        return True;
    else : return False
