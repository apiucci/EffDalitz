/*!
 *  @file      mean3DEff.cpp
 *  @author    Alessio Piucci
 *  @date      11-10-2017
 *  @brief     Script to compute the mean efficiency over a 2-dimensional distribution
 *  @return    Create an output info file with the efficiency
 */

//from std
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//from ROOT
#include <TROOT.h>
#include "TFile.h"
#include "TTree.h"
#include <TEntryList.h>
#include <TEfficiency.h>

#include "commonLib.h"
#include <IOjuggler.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

using namespace std;
namespace pt = boost::property_tree;

int main(int argc, char** argv){

  //-------------------------//
  //  parse the job options  //
  //-------------------------//
  
  std::string inFileName_num = "";
  std::string inFileName_den = "";
  std::string outLog_name = "";
  std::string configFile_name = "";
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "i:o:c:d:h")) != -1){
    
    switch (ca){
      
    case 'i':
      inFileName_num = optarg;
      break;

    case 'd':
      inFileName_den = optarg;
      break;
      
    case 'o':
      outLog_name = optarg;
      break;

    case 'c':
      configFile_name = optarg;
      break;
      
    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-i : input file name." << std::endl;
      std::cout << "-d : OPTIONAL: input file name with all events (denominator)."
		<< " If not specified, is taken equal to the -i argument" << std::endl;
      std::cout << "-o : output file name." << std::endl;
      std::cout << "-c : config file name." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
      
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
    }  //switch (ca)                                                                 
  }  //while ((ca = getopt(argc, argv, "i:c:o")) != -1)
  
  //convert env variables, if needed
  inFileName_num = ParseEnvName(inFileName_num);
  configFile_name = ParseEnvName(configFile_name);
  outLog_name = ParseEnvName(outLog_name);

  //for the denominator: if it was not parsed from command line,
  //use the same TFile of the denominator
  if(inFileName_den == "")
    inFileName_den = inFileName_num;
  
  std::cout << "inFileName_num = " << inFileName_num << std::endl;
  std::cout << "inFileName_den = " << inFileName_den << std::endl;
  std::cout << "configFile_name = " << configFile_name << std::endl;
  std::cout << "outLog_name = " << outLog_name << std::endl;
  
  //check that everything is fine
  if((inFileName_num == "") || (outLog_name == "") || (configFile_name == "")){
    std::cout << "Error: some of the parsed options are not valid." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //parse the options from the config file
  pt::ptree configtree;
  pt::read_info(configFile_name, configtree);
  
  IOjuggler::auto_append_in_ptree(configtree);
  
  std::string inTreeName_num = configtree.get<std::string>("numerator.inTree_name");
  std::string cuts_num = configtree.get<std::string>("numerator.cuts");
  
  std::string inTreeName_den = configtree.get<std::string>("denominator.inTree_name");
  std::string cuts_den = configtree.get<std::string>("denominator.cuts");
  
  std::cout << "inTreeName_num = " << inTreeName_num << std::endl;
  std::cout << "cuts_num = " << cuts_num << std::endl;
  std::cout << "inTreeName_den = " << inTreeName_den << std::endl;
  std::cout << "cuts_den = " << cuts_den << std::endl;
  std::cout << std::endl;
  
  //open the input file containing the numerator
  TFile* inFile_num = TFile::Open(inFileName_num.c_str(), "READ");
  
  //open the numerator tree
  TTree* inTree_num = (TTree*) inFile_num->Get(inTreeName_num.c_str());
  
  if(inTree_num == nullptr){
    std::cout << "Error: tree_num does not exist." << std::endl;
    exit(EXIT_FAILURE); 
  }

  //open the input file containing the denominator
  TFile* inFile_den = TFile::Open(inFileName_den.c_str(), "READ");
  
  //open the denominator tree
  TTree* inTree_den = (TTree*) inFile_den->Get(inTreeName_den.c_str());
  
  if(inTree_den == nullptr){
    std::cout << "Error: tree_den does not exist." << std::endl;
    exit(EXIT_FAILURE); 
  }
  
  //I use a TEntryList to select the events on which I'm interested
  
  //get the numerator candidates
  inTree_num->Draw(">>list_events_num", cuts_num.c_str(), "entrylist");
  
  TEntryList *list_events_num = (TEntryList*) gDirectory->Get("list_events_num");
  
  double entries_num = list_events_num->GetN();
  
  //get the denominator candidates
  inTree_den->Draw(">>list_events_den", cuts_den.c_str(), "entrylist");
  
  TEntryList *list_events_den = (TEntryList*) gDirectory->Get("list_events_den");
  
  double entries_den = list_events_den->GetN();
  
  //compute the results
  double low_CL = TEfficiency::Wilson(entries_den, entries_num, 0.6832, false);
  double up_CL = TEfficiency::Wilson(entries_den, entries_num, 0.6832, true);
  
  double efficiency = entries_num / entries_den;
  
  double error = (up_CL - low_CL)/2.;
  double err_low = efficiency - low_CL;
  double err_up = up_CL - efficiency;
  
  //print the results
  std::cout << "entries_num = " << entries_num << std::endl;
  std::cout << "entries_den = " << entries_den << std::endl;
  std::cout << "efficiency = " << efficiency << " +- " << error << std::endl;
  
  //save the results in a output INFO file
  pt::ptree log_out;
  
  log_out.put<double>("efficiency", efficiency);
  log_out.put<double>("error", error);
  log_out.put<double>("error_up", err_up);
  log_out.put<double>("error_low", err_low);
  log_out.put<double>("num_passed", entries_den);
  log_out.put<double>("num_total", entries_num);
  
  //write the output .info file
  write_info(outLog_name, log_out);
  
  //close the input files
  inFile_num->Close();
  inFile_den->Close();
  
  return 0;
}
