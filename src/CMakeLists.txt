#//-------------------------------------------------------------------------
#//
#// Description:
#//      build file for the scripts to Dalitz plots
#//
#//
#// Author List:
#//      Alessio Piucci		Heidelberg University          (original author)
#//      Marian Stahl         Heidelberg
#//
#//-------------------------------------------------------------------------


message(STATUS "")
message(STATUS "${BoldWhite}>>> Setting up 'EffDalitz/src' directory.${ColourReset}")

#sanity check for required modules
if(NOT TH2A_FOUND)
  message(FATAL_ERROR "TH2A library has not been built. Aborting...")
endif()
if(NOT ROOT_FOUND)
  message(FATAL_ERROR "ROOT not found. Aborting...")
endif()
if(NOT Boost_FOUND)
  message(FATAL_ERROR "Boost not found. Aborting...")
endif()

#get git hash to be used for printing in executables (not used currently, but printout during cmake-ing already helpful)
if(GIT_FOUND)
  execute_process(
    COMMAND ${GIT_EXECUTABLE} --git-dir=${CMAKE_CURRENT_SOURCE_DIR}/../.git rev-parse HEAD
    OUTPUT_VARIABLE EFFD_GIT_HASH
    RESULT_VARIABLE _EFFD_GIT_LOG_RETURN
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if(NOT _EFFD_GIT_LOG_RETURN)
    message(STATUS "EffDalitz' git repository hash is '${EFFD_GIT_HASH}'.")
    add_definitions(-DEFFD_GIT_HASH=\"${EFFD_GIT_HASH}\")
  else()
    message(STATUS "Error running 'git'. Repository hash unknown.")
  endif()
  execute_process(
    COMMAND ${GIT_EXECUTABLE} --git-dir=$ENV{TH2A_DIR}/.git rev-parse HEAD
    OUTPUT_VARIABLE TH2A_GIT_HASH
    RESULT_VARIABLE _TH2A_GIT_LOG_RETURN
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if(NOT _TH2A_GIT_LOG_RETURN)
    message(STATUS "2DAdaptiveBinning's git repository hash is '${TH2A_GIT_HASH}'.")
    add_definitions(-DTH2A_GIT_HASH=\"${TH2A_GIT_HASH}\")
  else()
    message(STATUS "Error running 'git'. Repository hash unknown.")
  endif()
  execute_process(
    COMMAND ${GIT_EXECUTABLE} --git-dir=$ENV{TH2A_DIR}/IOjuggler/.git rev-parse HEAD
    OUTPUT_VARIABLE IOJ_GIT_HASH
    RESULT_VARIABLE _IOJ_GIT_LOG_RETURN
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if(NOT _IOJ_GIT_LOG_RETURN)
    message(STATUS "IOjuggler's git repository hash is '${IOJ_GIT_HASH}'.")
    add_definitions(-DIOJ_GIT_HASH=\"${IOJ_GIT_HASH}\")
  else()
    message(STATUS "Error running 'git'. Repository hash unknown.")
  endif()
endif()

#build libraries
add_library(commonLib ../include/commonLib.cpp)
target_include_directories(commonLib PUBLIC ${Boost_INCLUDE_DIRS})#PUBLIC transfers the include directories to executables using this lib
target_link_libraries(commonLib ${TH2A_MODULE})#IOjuggler and TH2A includes transferred from building the TH2A module

add_library(effLUT ../include/effLUT.cpp)
target_include_directories(effLUT PUBLIC ${Boost_INCLUDE_DIRS})
target_link_libraries(effLUT ${TH2A_MODULE})

add_library(Eff ../include/Eff.cpp)
target_include_directories(Eff PUBLIC ${Boost_INCLUDE_DIRS})
target_link_libraries(Eff ${TH2A_MODULE})

#executables
add_executable(make2DEff make2DEff.cpp)
target_include_directories(make2DEff PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(make2DEff ${ROOT_LIBRARIES} Eff commonLib)

add_executable(make2DEff_adaptive make2DEff_adaptive.cpp)
target_include_directories(make2DEff_adaptive PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(make2DEff_adaptive ${ROOT_LIBRARIES} Eff commonLib)

add_executable(make2DEff_adaptive_filtered make2DEff_adaptive_filtered.cpp)
target_include_directories(make2DEff_adaptive_filtered PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(make2DEff_adaptive_filtered ${ROOT_LIBRARIES} Eff commonLib)

add_executable(makeBDTEff makeBDTEff.cpp)
target_include_directories(makeBDTEff PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(makeBDTEff ${ROOT_LIBRARIES} ${TH2A_MODULE} commonLib)

add_executable(makePIDEff makePIDEff.cpp)
target_include_directories(makePIDEff PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(makePIDEff ${ROOT_LIBRARIES} ${TH2A_MODULE} commonLib)

add_executable(combPlots combPlots.cpp)
target_include_directories(combPlots PRIVATE ${Boost_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(combPlots ${ROOT_LIBRARIES})

add_executable(mean2DEff mean2DEff.cpp)
target_include_directories(mean2DEff PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(mean2DEff ${ROOT_LIBRARIES} commonLib)

add_executable(make1DEff make1DEff.cpp)
target_include_directories(make1DEff PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(make1DEff ${ROOT_LIBRARIES} commonLib)

add_executable(yield yield.cpp)
target_include_directories(yield PRIVATE ${Boost_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../include)
target_link_libraries(yield effLUT commonLib ${ROOT_LIBRARIES})
